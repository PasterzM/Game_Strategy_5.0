#ifndef SELECTION_H_
#define SELECTION_H_
#include <SFML\Graphics.hpp>

//TODO: nie potrzebne dziedzicznie
class Selection :public sf::RectangleShape{
public:
	static void Stworz();
	static void Destroy();
	static Selection& GetObiekt();

	void start(sf::Vector2f &mouse);
	void stop(sf::Vector2f &mouse);
	sf::RectangleShape& getSelectionArea();

	bool& isSelectionMode();

	void update(sf::Vector2f &mouse);

	void draw(sf::RenderTarget &, sf::RenderStates) const;
private:
	static Selection *Obiekt;

	Selection();

	sf::RectangleShape ObszarZaznaczenia;
	bool m_b_isSelection;
};

#endif