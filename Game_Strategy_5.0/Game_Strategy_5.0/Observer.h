#ifndef OBSERVER_H_
#define OBSERVER_H_
#include "Observed.h"

class Observed;

//obserwator
class Observer {
public:
	////////////////////////////////
	//	wskazanie obiektu obserwowanego
	///////////////////////////////
	void SetObserver(Observed *pSub);
	////////////////////////////////
	//	komunikowanie obiektu obserwowanego a obserwatora
	////////////////////////////////
	virtual void AktualizujObiektObserwowany() = 0;
	~Observer();
	///////////////////////////////
	//Zwraca wskaznik na obikt obserwowany
	///////////////////////////////
	Observed* getObserwowany() {
		return ObserwowanyObiekt;
	}
protected:
	Observed *ObserwowanyObiekt;
};

#endif