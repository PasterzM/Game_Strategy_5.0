#include "LinesMaps.h"

LinesMaps::LinesMaps(Map & map) :Prostokaty(NULL), isLinieMapy(true) {
}

LinesMaps::~LinesMaps() {
	if (Prostokaty) {
		for (int i = 0; i < v_height; i++)	delete[] Prostokaty[i];
		delete[] Prostokaty;
	}
}

void LinesMaps::SetViewLines(bool arg) {
	isLinieMapy = arg;
}

bool& LinesMaps::GetWidocznoscLinij() {
	return isLinieMapy;
}

void LinesMaps::SetMaps(Map & map) {
	v_width = map.GetDlugosc();
	v_height = map.GetWysokosc();

	if (Prostokaty) {
		for (int i = 0; i < v_height; i++)	delete[] Prostokaty[i];
		delete[] Prostokaty;
	}
	Prostokaty = new sf::RectangleShape*[v_height];
	for (int i = 0; i < v_height; i++)	Prostokaty[i] = new sf::RectangleShape[v_width];

	for (int i = 0; i < v_height; i++) {
		for (int j = 0; j < v_width; j++) {
			Prostokaty[i][j].setSize((sf::Vector2f)map.GetWielkoscPlytki());

			sf::Vector2f pozycja(map.GetWielkoscPlytki().x*i, map.GetWielkoscPlytki().y*j);
			Prostokaty[i][j].setPosition(pozycja);

			sf::Color kolorWew(0,0,0,0);
			Prostokaty[i][j].setFillColor(kolorWew);

			sf::Color kolorZew(0, 0, 0, 100);
			Prostokaty[i][j].setOutlineColor(kolorZew);

			//Prostokaty[i][j].setOutlineThickness(map.GetWielkoscPlytki().x);
			Prostokaty[i][j].setOutlineThickness(1.0f);
		}
	}
}

void LinesMaps::draw(sf::RenderTarget &target, sf::RenderStates states) const {
	for (int i = 0; i < v_height; i++) {
		for (int j = 0; j < v_width; j++) {
			target.draw(Prostokaty[i][j]);
		}
	}
}