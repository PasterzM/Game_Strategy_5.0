#include "Player.h"

int Player::v_counter = 0;

Player::Player(string & name, int gold, int metal, int pop, int points) :v_name(name), v_gold(gold), v_metal(metal), v_availablePopulation(pop), v_points(points),v_population(0),v_maxPopulation(v_availablePopulation) {
	v_id = v_counter;
	v_counter++;
}

int Player::GetId() const {
	return v_id;
}

string Player::GetName() const {
	return v_name;
}

int  Player::GetGold()const {
	return v_gold;
}

int  Player::GetMetal()const {
	return v_metal;
}

int  Player::GetPopulation() const{
	return v_population;
}

int  Player::GetMaxPopulation()const {
	return v_maxPopulation;
}

int  Player::GetAvailablePopulation()const {
	return v_availablePopulation;
}

bool Player::AddGold(const int arg) {
	if (v_gold + arg >= 0) {
		v_gold += arg;
		return true;
	}
	return false;
}

bool Player::AddMetal(const int arg) {
	if (v_metal + arg >= 0) {
		v_metal += arg;
		return true;
	}
	return false;
}

bool Player::AddPop(const int arg) {
	if (v_population + arg >= 0) {
		v_population += arg;
		return true;
	}
	return false;
}

bool Player::AddPoints(const int arg) {
	if (v_points + arg >= 0) {
		v_points += arg;
		return true;
	}
	return false;
}

bool Player::SetPopulation(const int arg) {
	if (arg >= 0) {
		v_population = arg;
		return true;
	}
	return false;
}

bool Player::SetMaxPopulation(const int arg) {
	if (arg >= 0) {
		v_maxPopulation = arg;
		return true;
	}
	return false;
}

bool Player::AddToPopulation(const int arg) {
	if (v_population + arg >= 0 && v_population + arg <= v_availablePopulation) {
		v_population += arg;
		return true;
	}
	return false;
}

bool Player::AddToAvailablePopulation(const int arg) {
	if (v_availablePopulation + arg >= 0 && v_availablePopulation + arg <= v_maxPopulation) {
		v_availablePopulation += arg;
		return true;
	}
	return false;
}

bool Player::SetAvailablePopulation(const int arg) {
	if (arg >= 0) {
		v_availablePopulation += arg;
		return true;
	}
	return false;
}