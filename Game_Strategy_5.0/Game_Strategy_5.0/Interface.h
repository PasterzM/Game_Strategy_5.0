#ifndef INTERFACE_H_
#define INTERFACE_H_
#include <SFML\Graphics.hpp>
#include "Button.h"
#include "Table.h"
#include "PrintObiect.h"
#include "PrintObiect.h"

class Interface {
public:
	Interface();
	Interface(int numberOfButtons,bool isPanel);
	~Interface();

	bool isPanelView();
	void SetIsPanelView(bool arg);

	sf::View& GetGameView();
	sf::View& GetPanelView();
	sf::View& GetMiniMapView();


	void Draw(sf::RenderWindow& win);
	void DrawPlayer(sf::RenderWindow& win);
	Table<Button>& GetButtonTable();
	void SetButtonsType(vector<string>& obiButtons,map<string,Button*>& buttonManadzer);

	void SetObiectToView(Obiect* obi);
	PrintObiect& GetObiectToView();
	void SetPlayerToDraw(Player* arg);

	sf::Text& GetPlayerGold();
	sf::Text& GetPlayerMetal();
	sf::Text& GetPlayerFood();
private:
	bool v_isPanelView;
	
	sf::View v_gameView;
	sf::View v_panelView;
	sf::View v_miniMap;

	Table<Button> v_pButton;

	int ButtonsCounter();
	void InitButtonsPosition();

	static string v_s_fileButtonsName;

	//Obiect* p_obiectToView;

	PrintObiect v_printObi;
	Obiect* p_obiectToView;

	//Gracz ktorego nalezy wyswietlic
	Player* p_player;

	sf::Font v_font;
	sf::Text v_playerGold;
	sf::Text v_playerMetal;
	sf::Text v_playerFood;
};
#endif