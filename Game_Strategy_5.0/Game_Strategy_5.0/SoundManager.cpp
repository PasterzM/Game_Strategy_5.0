#include "SoundManager.h"
#include "Functions.h"
#include <algorithm>

using namespace std;

SoundManager* SoundManager::p_obiect = NULL;

void SoundManager::Create() {
	if (p_obiect)
		delete p_obiect;
	p_obiect = new SoundManager;
}

void SoundManager::Destroy() {
	if (p_obiect) {
		delete p_obiect;
	}
	p_obiect = NULL;
}

bool SoundManager::AddSound(std::string name, std::string path) {
	sf::SoundBuffer soundBuffer;
	if (soundBuffer.loadFromFile(path)) {
		if (v_m_SoundBuffers.find(name) != v_m_SoundBuffers.end()) {
			v_m_SoundBuffers.at(name) = new sf::SoundBuffer(soundBuffer);
			v_m_Sounds.at(name) = new sf::Sound(*v_m_SoundBuffers.at(name));
		} else {
			v_m_SoundBuffers[name] = new sf::SoundBuffer(soundBuffer);
			v_m_Sounds[name] = new sf::Sound(*v_m_SoundBuffers.at(name));
		}
		return true;
	}
	return false;
}

SoundManager * SoundManager::GetManadzer() {
	return p_obiect;
}

sf::Sound* SoundManager::GetSound(string name) {
	if (v_m_Sounds.find(name) != v_m_Sounds.end())
		return v_m_Sounds.at(name);
	else
		return 0;
}

map<string, sf::Sound*>& SoundManager::GetMap() {
	return v_m_Sounds;
}

SoundManager::SoundManager() {
}



SoundManager::~SoundManager() {
	//for_each(v_m_ButtonManager.begin(), v_m_ButtonManager.end(), deletesMap<string, Button*>);
	for_each(v_m_SoundBuffers.begin(), v_m_SoundBuffers.end(), deletesMap<string, sf::SoundBuffer*>);
	for_each(v_m_Sounds.begin(), v_m_Sounds.end(), deletesMap<string, sf::Sound*>);

	v_m_SoundBuffers.clear();
	v_m_Sounds.clear();
}