#include <imgui.h>
#include <imgui-SFML.h>
#include <iostream>
#include "Functions.h"

using namespace std;

#include "Game.h"
#include "Table.h"

GameStatus Game::StatusGry = MENU;
map<string, string>::iterator Game::it_selectedMap;
LinesMaps Game::linesMaps;

string Game::v_s_FileButtonRectType("filesTxt/Button Rect and Type.txt");

static Table<ObiectNameSpace::ObiectDirection> direction(9);


using namespace MouseNameSpace;

Game::Game() : v_window(sf::VideoMode(800, 600), "Menu Gry"), L(NULL), v_EditorWindow(NULL) {
	StatusGry = MENU;

	direction[0] = ObiectNameSpace::STOP;
	direction[1] = ObiectNameSpace::N;
	direction[2] = ObiectNameSpace::NE;
	direction[3] = ObiectNameSpace::E;
	direction[4] = ObiectNameSpace::SE;
	direction[5] = ObiectNameSpace::S;
	direction[6] = ObiectNameSpace::WS;
	direction[7] = ObiectNameSpace::W;
	direction[8] = ObiectNameSpace::NW;

	L = luabridge::luaL_newstate();
	luaL_openlibs(L);

	//wczytywanie plikow gry
	LoadFile();
	ShowParameter(cout);

	do {
		switch (StatusGry) {
		case MENU:	Menu();	break;
		case PLAY: GlownaPetla();	break;
		case RESTARTMAP: GlownaPetla();	break;
		case RESTART: 	return;	break;
		case EXIT: return;	break;
		}
	} while (StatusGry == MENU || StatusGry == PLAY || StatusGry == RESTARTMAP);
}

Game::~Game() {
	if (v_EditorWindow)	delete v_EditorWindow;
	v_EditorWindow = NULL;
	Selection::Destroy();
	MessageManager::Destroy();

	for_each(v_m_ButtonManager.begin(), v_m_ButtonManager.end(), deletesMap<string, Button*>);
	v_m_ButtonManager.clear();

	for_each(Paterns.begin(), Paterns.end(), deletesMap<string, Obiect*>);
	Paterns.clear();

	MessageManager::Destroy();
	SoundManager::Destroy();
	ManadzerTexture::Remove();

}

void Game::GlownaPetla() {
	system("cls");
	sf::Clock clock;
	sf::Time dt;

	StatusGry = PLAY;
	v_MouseType = MouseType::NOTHING;

	LoadTexture();
	InitScripts();	//inicjalizacja skrpytow i wzorow obiektow
	InitPlayers();
	InitButtons();
	InitSoundManager();
	InitEditor();
	InitMap();
	InitMessageManager();
	v_AStar.SetMap(Map::GetObiekt());

	//TODO:je�eli tryb debugowania
	int pos_x = v_EditorWindow->GetWindow().getSize().x;
	v_window.setPosition(sf::Vector2i(pos_x + 20, 0));
	while (v_window.isOpen()) {
		dt = clock.restart();
		Wejscie();
		Aktualizuj(dt);
		Draw();

		switch (StatusGry) {
		case RESTARTMAP:
			saveConfiguration();
			return;
			break;
		case RESTART:
			saveConfiguration();
			return;
			break;
		case EXIT:
			saveConfiguration();
			return;
			break;
		}
	}
}

void Game::Wejscie() {
	sf::Event event;
	while (v_window.pollEvent(event)) {
		switch (event.type) {
		case sf::Event::Closed:
		{
			StatusGry = EXIT;
			v_window.close();
			break;
		}
		case sf::Event::MouseButtonPressed:
		{
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
				switch (v_MouseType) {
				case MouseType::NOTHING:
				{
					v_MouseType = MouseType::SELECTED;
					sf::Vector2f posMouse(v_window.mapPixelToCoords(sf::Mouse::getPosition(v_window)));
					Selection::GetObiekt().start(posMouse);
					break;
				}
				}
			}
			break;
		}
		case sf::Event::MouseButtonReleased:
		{
			switch (v_MouseType) {
			case MouseType::SELECTED: {
				v_MouseType = MouseType::NOTHING;
				sf::Vector2f posMouse(v_window.mapPixelToCoords(sf::Mouse::getPosition(v_window)));
				Selection::GetObiekt().update(posMouse);
				Selection::GetObiekt().stop(posMouse);

				v_EditorWindow->ClearSelectedObiect();

				v_vec_p_Obiects = v_vec_SelectedObiects.begin();
				while (v_vec_p_Obiects != v_vec_SelectedObiects.end()) {
					(*v_vec_p_Obiects)->setSelected(false);
					//cout << it_vec_obiects->Getcircle().getFillColor().r << " " << it_vec_obiects->Getcircle().getFillColor().g << " " << it_vec_obiects->Getcircle().getFillColor().b << endl;
					++v_vec_p_Obiects;
				}
				v_vec_SelectedObiects.clear();

				//zaznaczenie obiektow
				sf::RectangleShape selected(Selection::GetObiekt().getSelectionArea());
				it_vec_obiects = v_vec_Obiects.begin();
				while (it_vec_obiects != v_vec_Obiects.end()) {
					if (isCollision(*it_vec_obiects, selected)) {	//jezeli obiekt znajduje sie w strefie zanaczenia
						v_vec_SelectedObiects.push_back(&(*it_vec_obiects));
						it_vec_obiects->setSelected(true);
						//cout << it_vec_obiects->Getcircle().getFillColor().r << " " << it_vec_obiects->Getcircle().getFillColor().g << " " << it_vec_obiects->Getcircle().getFillColor().b << endl;
					}
					++it_vec_obiects;
				}

				if (v_vec_SelectedObiects.size() == 1) {
					v_EditorWindow->SetObiectToEdit(*v_vec_SelectedObiects.begin());
					//TODO: ustawienie wszystkich wartosci w edytorze na wartosci obiektu zaznaczonego !! BARDZO WAZNE !!
					v_interface.SetButtonsType((*v_vec_SelectedObiects.begin())->GetListOfButtons(), v_m_ButtonManager);
				}

				break;
			}
			case MouseType::NOTHING: {
				//wyznacza sciezke tylko jezeli cos jest zaznaczone i pierwszy element nie jest budynkiem oraz jednostka ma id numeru gracza
				if (!v_vec_SelectedObiects.empty() && !(*v_vec_SelectedObiects.begin())->GetIsBuilding()) {
					v_MouseType = MouseType::NOTHING;
					sf::Vector2i posMouse(v_window.mapPixelToCoords(sf::Mouse::getPosition(v_window)));
					posMouse.x /= Map::GetWielkoscPlytki().x;
					posMouse.y /= Map::GetWielkoscPlytki().y;

					AStarInitWithObiects init;
					for (v_vec_p_Obiects = v_vec_SelectedObiects.begin(); v_vec_p_Obiects != v_vec_SelectedObiects.end(); ++v_vec_p_Obiects) {
						sf::Vector2i start((*v_vec_p_Obiects)->GetPosition());
						start.x /= Map::GetWielkoscPlytki().x;
						start.y /= Map::GetWielkoscPlytki().y;
						(*v_vec_p_Obiects)->SetPath(*v_AStar.SzukajSciezki(start, posMouse, init));
					}
				}
			}
			}
			break;
		}
		case sf::Event::MouseMoved:
		{
			//TODO: tytul okna ma wyswietlac pozycje myszki w trybie debug
			/*
			sf::Vector2i pos(sf::Mouse::getPosition(v_window));
			string text( pos.x );
			v_window.setTitle(  );
			*/
			switch (v_MouseType) {
			case MouseType::SELECTED:
			{
				sf::Vector2f posMouse(v_window.mapPixelToCoords(sf::Mouse::getPosition(v_window)));
				Selection::GetObiekt().update(posMouse);
				break;
			}
			}
			break;
		}
		case sf::Event::KeyPressed: {	//Obsluga klawiatury
			switch (event.key.code) {	//przesuwanie widoku gry  lewo,prawo,gora,dol
			case sf::Keyboard::Left:	v_interface.GetGameView().move(-Map::GetWielkoscPlytki().x, 0);	break;
			case sf::Keyboard::Right:	v_interface.GetGameView().move(Map::GetWielkoscPlytki().x, 0);	break;
			case sf::Keyboard::Down:	v_interface.GetGameView().move(0, Map::GetWielkoscPlytki().y);	break;
			case sf::Keyboard::Up:	v_interface.GetGameView().move(0, -Map::GetWielkoscPlytki().y);	break;
			}

			break;
		}
		}

	}
}

void Game::Draw() {
	v_window.clear();

	if (v_interface.isPanelView()) {
		v_interface.DrawPlayer(v_window);
		//panel boczny
		v_window.setView(v_interface.GetPanelView());
		if (v_vec_SelectedObiects.size() == 1) {
			(*v_vec_SelectedObiects.begin())->DrawInterface(v_window);
			v_interface.Draw(v_window);
		}

		//minimapa
		v_window.setView(v_interface.GetMiniMapView());
		Map::GetObiekt().Rysuj(v_window);	//mapa

		for each (Obiect var in v_vec_Obiects) {
			var.draw(v_window);
		}
	}

	v_window.setView(v_interface.GetGameView());

	if (v_EditorWindow) {
		v_EditorWindow->ExecuteEditor();
		if (strcmp(v_EditorWindow->GetScript().c_str(), "") != 0) {
			string scriptName(v_EditorWindow->reset());
			executeScript(scriptName);
		}
	}

	//v_window.clear(sf::Color::Black);
	Map::GetObiekt().Rysuj(v_window);	//mapa
	if (linesMaps.GetWidocznoscLinij()) {	//linie mapy
		v_window.draw(linesMaps);
	}

	it_vec_obiects = v_vec_Obiects.begin();
	while (it_vec_obiects != v_vec_Obiects.end()) {
		it_vec_obiects->draw(v_window);
		++it_vec_obiects;
	}

	if (Selection::GetObiekt().isSelectionMode()) {	//zaznaczenie
		v_window.draw(Selection::GetObiekt());
	}

	v_window.display();
}

void Game::Aktualizuj(sf::Time & dt) {
	for (it_vec_obiects = v_vec_Obiects.begin(); it_vec_obiects != v_vec_Obiects.end(); ++it_vec_obiects) {
		it_vec_obiects->Update(dt);
	}
}

void Game::InitMap() {
	Map::Stworz();
	Map::GetObiekt().CreateMap(it_selectedMap->second, 32);
	linesMaps.SetMaps(Map::GetObiekt());
	linesMaps.SetViewLines(true);
	Selection::Stworz();

	//TODO: chwilowa inicjalizacja mapy obiektami
	Player* player(&(*v_vec_Players.begin()));
	v_vec_Obiects.push_back(Obiect(Paterns.at("Builder"), 32 * 4, 32 * 4, *player));
	v_vec_Obiects.push_back(Obiect(Paterns.at("Builder"), 32 * 6, 32 * 6, *player));
	v_vec_Obiects.push_back(Obiect(Paterns.at("Barracks"), 32 * 4, 32 * 8, *player));

	v_vec_Obiects.push_back(Obiect(Paterns.at("Base"), 32 * 4, 32 * 20, *player));

	player = &v_vec_Players.back();
	v_vec_Obiects.push_back(Obiect(Paterns.at("GoldMine"), 32 * 4, 32 * 14, *player));

	if (v_EditorWindow) {
		v_EditorWindow->addTextToConsole("create obiect " + v_vec_Obiects.back().GetName());
	}

	//oznaczenie obszarow mapy jako zajete przez obiekty
	Map* p = &Map::GetObiekt();
	for (it_vec_obiects = v_vec_Obiects.begin(); it_vec_obiects != v_vec_Obiects.end(); ++it_vec_obiects) {
		sf::Vector2i pos(it_vec_obiects->GetPosition());
		int size(it_vec_obiects->GetSize());

		pos.x /= p->GetWielkoscPlytki().x;
		pos.y /= p->GetWielkoscPlytki().y;

		size /= p->GetWielkoscPlytki().x;
		p->getPlytka(pos).Wsk = &(*it_vec_obiects);

		for (int x = pos.x; x < pos.x + size; ++x)
			for (int y = pos.y; y < pos.y + size; ++y) {
				p->getPlytka(x, y).Wsk = &(*it_vec_obiects);
			}
	}

	//Ustalanie widoku gry
	//v_interface.GetPanelView().setSize(800 * 0.3, 600);
	v_interface.GetPanelView().setViewport(sf::FloatRect(0, 0, 1.0f, 1.0f));
	v_interface.GetGameView().setViewport(sf::FloatRect(0.3f, 0.0f, 1.0f, 1.0f));
	v_interface.GetMiniMapView().setViewport(sf::FloatRect(0.01, 0.01, 0.28, 0.28));
}

void Game::InitScripts() {
	//game scripts
	using namespace luabridge;
	L = luaL_newstate();
	luaL_openlibs(L);

	string fileName("Scripts/Game.lua");

	//Skrypty gry oraz eytora
	getGlobalNamespace(L)
		.beginClass<Game>("Window")
		.addConstructor<void(*)(void)>()
		//.addData("intTable", &Game::table)
		.addFunction("reload", &Game::InitScripts)
		.addFunction("printListofGameScripts", &Game::printListofGameScripts)
		.addFunction("printListOfGameObiects", &Game::printListOfGameObiects)
		.addFunction("LoadScriptFunction", &Game::LoadScriptfunctionFromWindow)
		.addFunction("addTextToConsole", &Game::addTextToConsole)
		.endClass()
		.beginClass<vector<Obiect>>("VecObiect")
		.addConstructor<void(*)()>()
		.addFunction<void (vector<Obiect>::*)(const Obiect&)>("push", &vector<Obiect>::push_back)
		//.addProperty("Paterns",&Game::GetPaterns)
		.endClass();

	if (!luaL_dofile(L, fileName.c_str())) {
		if (!LoadScriptfunctionFromWindow("LoadScriptFunction"))	cout << "error with Game::" << *it_list_String << endl;
		if (!LoadScriptfunctionFromWindow("InitScripts"))	cout << "error with Game::" << *it_list_String << endl;
	} else {
		std::cout << "Error, can't open script! " << fileName << std::endl;
	}

	//inicjalizacja skryptow gry
	(*LuaFunc.at("InitScripts"))(this);

	//rejestrowanie klasy Obiekt
	Obiect::RegisterClassObiect(L);

	//towrzenie wzorow obiektow gry jezeli jest blad w skrypcie obiektu, nie zostanie on stworzony
	for (it_scripts = map_scriptMenager.begin(); it_scripts != map_scriptMenager.end(); ++it_scripts) {
		try {
			Paterns[it_scripts->first] = new Obiect(it_scripts->first, it_scripts->second);
			//Paterns.back().printData();
			if (Paterns.at(it_scripts->first)->GetName() == "") {
				Paterns.erase(it_scripts->first);
			}
			Paterns.at(it_scripts->first)->printData();
		} catch (exception e) {
			//Paterns.pop_back();
			cout << "/nBlad pliku ze skryptem : /n" << it_scripts->first << endl;
		}
	}

	//otwieranie plikow wzorow i wczytywanie danych dotyczacych obiektow
	fstream file;
	for (map<string, Obiect*>::iterator it = Paterns.begin(); it != Paterns.end(); ++it) {
		Obiect* obi = it->second;
		file.open("filesTxt/" + obi->GetName() + ".txt", ios::in);
		if (file.is_open()) {
			string junk;
			sf::IntRect rect;
			if (!it->second->GetIsBuilding()) {
				//rect ikony
				file >> junk >> junk;
				file >> rect.left >> rect.top >> rect.width >> rect.height;
				obi->GetSriteIcon().setTextureRect(rect);

				//Animacja
				file >> junk >> junk;
				for (int i = 0; i < direction.m_length; ++i) {
					for (int j = 0; j < it->second->GetFrameAnimationTable()[direction[i]]->m_length; ++j) {
						sf::IntRect rect;
						file >> junk >> junk >> rect.left >> rect.top >> rect.width >> rect.height;
						(*it->second->GetFrameAnimationTable()[i])[j] = rect;
					}
				}
				//predkosc animacji
				float speed = 0;
				file >> junk >> junk >> speed;
				it->second->SetMoveAnimationSpeed(speed);

				//Walka
				file >> junk >> junk;
				for (int i = 0; i < direction.m_length; ++i) {
					for (int j = 0; j < it->second->GetFrameAttackTable()[direction[i]]->m_length; ++j) {
						sf::IntRect rect;
						file >> junk >> junk >> rect.left >> rect.top >> rect.width >> rect.height;
						(*it->second->GetFrameAttackTable()[i])[j] = rect;
					}
				}

				//Smierc
				file >> junk >> junk;
				for (int i = 0; i < direction.m_length; ++i) {
					for (int j = 0; j < it->second->GetFrameDeathTable()[direction[i]]->m_length; ++j) {
						sf::IntRect rect;
						file >> junk >> junk >> rect.left >> rect.top >> rect.width >> rect.height;
						(*it->second->GetFrameDeathTable()[i])[j] = rect;
					}
				}

			} else {	//jezeli jest budynkiem
				sf::IntRect rect;
				//rect ikony
				file >> junk >> junk;
				file >> rect.left >> rect.top >> rect.width >> rect.height;
				obi->GetSriteIcon().setTextureRect(rect);
				//sprite
				file >> junk >> junk >> rect.left >> rect.top >> rect.width >> rect.height;
				it->second->GetSprite().setTextureRect(rect);


			}
			file >> junk;
			file >> junk;
			file >> junk;
			it->second->SetSpritePath(junk);

			if (junk != ":") {
				string texturePath(junk.substr(ManadzerTexture::v_s_TexturePath.size() + 1, junk.size() - ManadzerTexture::v_s_TexturePath.size()));
				if (ManadzerTexture::GetManadzer()->GetMap().find(texturePath) != ManadzerTexture::GetManadzer()->GetMap().end())
					it->second->SetSprite(*ManadzerTexture::GetManadzer()->GetMap().at(texturePath));
			}

			file.close();
		} else {
			string error("nie udalo sie otworzyc pliku o nazwie " + obi->GetName() + ".txt");
			cout << error << endl;
			//throw exception(error.c_str());
		}
	}

	file.open("filesTxt/Game.txt", ios::in);
	if (file.is_open()) {
		string junk;

		//rect ikony
		sf::Vector2f vec;
		file >> junk >> junk;
		file >> vec.x >> vec.y;

		//skalowanie obiektu
		sf::Vector2f scale;
		file >> junk >> junk;
		file >> scale.x >> scale.y;

		file >> junk >> junk;
		file >> v_numberOfAnimationInBuild;

		for (map<string, Obiect*>::iterator it = Paterns.begin(); it != Paterns.end(); ++it) {
			it->second->GetSriteIcon().setPosition(vec);
			it->second->GetSriteIcon().setScale(scale);
			//it->second->SetSpritesNumberAnimation(v_numberOfAnimationInBuild);
		}
		file.close();
	} else {
		string error("nie udlao sie ptworzyc pliku o nazwie Game.txt");
		cout << error << endl;
		throw exception(error.c_str());
	}
}

void Game::InitButtons() {
	//przyciski z interfejsu
	Table<Button>* pTable = &v_interface.GetButtonTable();
	sf::Texture* pIconTexture = ManadzerTexture::GetManadzer()->GetMap().at("Icons");

	//twotrzenie roznych typow przyciskow
	if (pIconTexture) {
		//dla kazdego wzoru obiektu jest nadawana jakas textura oraz jej kordynaty z pliku
		for (it_map_obiect = Paterns.begin(); it_map_obiect != Paterns.end(); ++it_map_obiect) {
			v_m_ButtonManager[it_map_obiect->first] = new Button(it_map_obiect->first, *pIconTexture, ButtonType::BUILDING);

			if (!it_map_obiect->second->GetIsBuilding()) {	//jezeli obiekt jest typu jednostki to ustawia typ przycisku na jednostke
				v_m_ButtonManager[it_map_obiect->first]->SetType(ButtonType::UNIT);
			}
		}
	} else {
		throw exception("Brak textury \"Icons\"");
	}
	v_m_ButtonManager["None"] = new Button("None", *pIconTexture, ButtonType::BUILDING);

	//odczytanie danych dotyczacych przyciskow z pliku

	fstream file;

	file.open(v_s_FileButtonRectType, ios::in);
	if (file.is_open()) {
		sf::FloatRect rect;
		string name;
		string buffer;
		file >> buffer >> buffer >> buffer >> buffer;
		for (map<string, Button*>::iterator it = v_m_ButtonManager.begin(); it != v_m_ButtonManager.end(); ++it) {
			file >> rect.left >> rect.top >> rect.width >> rect.height;
			file >> name;
			if (v_m_ButtonManager.find(name) != v_m_ButtonManager.end())
				v_m_ButtonManager[name]->setTextureRect((sf::IntRect)rect);
		}
		file.close();
	}


	it_map_obiect = Paterns.begin();	//wazne nie komentowac, nie pamietam po co, ale jest wazne


	//TODO:inicjalizacja przyciskow zerami
	for (int i = 0; i < (*pTable).m_length; ++i) {
		pTable->operator[](i).setTexture(*pIconTexture);
		//TODO: inicjalizacja przyciskow 0
		pTable->operator[](i).setTextureRect(sf::IntRect(0, 0, 50, 50));
	}

	//inicjalizacja przyciskow kazdego ze wzorow obiektow
}

void Game::InitMessageManager() {	//wypisuje w oknie edytora wszystkie typy 

	MessageManager::Create(L);
	MessageManager* p = MessageManager::GetObject();

	p->AddMessageType("StartAttack", &Game::StartAttacking);
	p->AddMessageType("ProvidedResources", &Game::ProvidedResources);
	p->AddMessageType("EndDigging", &Game::EndDigging);
	p->AddMessageType("CreateObject", &Game::CreateObject);
	p->AddMessageType("KillObject", &Game::KillObject);


	//wypisuje w oknie edytora wszystkie typy 
	if (v_EditorWindow) {
		v_EditorWindow->addTextToConsole("Typy wiadomosci : ---------------");
		for each (std::pair<string, MetodyGame> var in p->GetMesssageType()) {
			v_EditorWindow->addTextToConsole(var.first);
		}
		v_EditorWindow->addTextToConsole("---------------------------------");
	}


}

void Game::LoadFile() {
	map_MapMenager["Mission01"] = "Maps/Mission01.txt";
	map_MapMenager["Mission02"] = "Maps/Mission02.txt";
	map_MapMenager["Mission03"] = "Maps/Mission03.txt";
	map_MapMenager["Mission04"] = "Maps/Mission04.txt";
	map_MapMenager["Mission05"] = "Maps/Mission05.txt";
	map_MapMenager["Mission06"] = "Maps/Mission06.txt";

	map_scriptMenager["Obiect"] = "Scripts/Obiect.lua";
	map_scriptMenager["Builder"] = "Scripts/Builder.lua";
	map_scriptMenager["Barracks"] = "Scripts/Barracks.lua";
	map_scriptMenager["GoldMine"] = "Scripts/GoldMine.lua";
	map_scriptMenager["MetalMine"] = "Scripts/MetalMine.lua";
	map_scriptMenager["Builder"] = "Scripts/Builder.lua";
	map_scriptMenager["Farm"] = "Scripts/Farm.lua";
	map_scriptMenager["Base"] = "Scripts/Base.lua";

	list_gameDictionary.push_back("win:");
	list_gameDictionary.push_back("Win:");
	list_gameDictionary.push_back("window:");
	list_gameDictionary.push_back("Window:");
	list_gameDictionary.push_back("game:");
	list_gameDictionary.push_back("Game:");


}

ostream & Game::ShowParameter(ostream & out) {
	out << "map files : " << endl;
	for (it_maps = map_MapMenager.begin(); it_maps != map_MapMenager.end(); ++it_maps) {
		out << it_maps->second << ", ";
	}out << endl;
	return out;
}

bool Game::LoadScriptfunctionFromWindow(string tab) {
	luabridge::LuaRef table = getGlobal(L, "Window");
	if (table.isTable() && table[tab].isFunction()) {
		if (LuaFunc.find(tab) == LuaFunc.end()) {
			LuaFunc[tab] = std::make_shared<luabridge::LuaRef>(table[tab]);
			cout << "Udalo sie wczytac skrypt " << tab << endl;
			if (v_EditorWindow)
				v_EditorWindow->addTextToConsole("Udalo sie wczytac skrypt " + tab);
			return true;
		} else {	//jezeli skrypt jest juz wczytany usun a nastepnie ponownie wstaw
			//LuaFunc[tab]->
			LuaFunc.erase(tab);
			LoadScriptfunctionFromWindow(tab);
		}
	}
	return false;
}

void Game::executeScript(string& c_script) {
	string script;
	for (int i = 0; i < c_script.length(); i++) {//wyciaganie podciagu wraz ze zenakiem :
		if (c_script.at(i) != ':') {
			script += c_script.at(i);
		} else {
			script += c_script.at(i);
			break;
		}
	}

	//sprawdzenie slownika czy skrypt odnosi sie do samej gry
	for (list<string>::iterator it = list_gameDictionary.begin(); it != list_gameDictionary.end(); ++it) {
		if (!script.find(*it)) {
			executeGameScript(c_script, it);
			return;
		}
	}

	//TODO: cos tu bylo do zrobienia !! uruchomienie skrypt�w do gry
	/*for (map<string, Obiect*>::iterator it = Paterns.begin(); it != Paterns.end(); ++it) {

	}*/

	std::cout << "Blad, niema takiego skryptu: " << script << std::endl;
}

void Game::executeGameScript(string & script, list<string>::iterator& it) {
	//usuwanie skrotu do gry
	string exec(script.substr((*it).size(), script.size()));
	bool isLoadScript = false;

	for (int i = 0; i < script.size(); ++i) {
		if (script.at(i) == '(') {
			isLoadScript = true;
			break;
		}
	}

	//jezeli w funkcji niema nawiasow to znaczy ze uruchamiany jest skrypt bezargumentowy
	try {
		if (!isLoadScript) {
			if (LuaFunc.at(exec)) {
				(*LuaFunc.at(exec))(this);
			}
		} else {
			//wyodrebnianie skryptu do wykonania
			int firstBracketPos(exec.find_first_of('('));
			int LastBracketPos(exec.find_last_of(')'));
			string instruction(exec.substr(0, firstBracketPos));

			if (instruction == "LoadScriptFunction" && LuaFunc.at("LoadScriptFunction")) {
				string name(exec.substr(0, 18));
				string arg(exec.substr(name.length() + 1, exec.length() - name.length() - 2));
				(*LuaFunc.at(name))(this, arg);
			} else if (instruction == "InitMap" && LuaFunc.at("InitMap")) {
				string name(exec.substr(0, 18));
				(*LuaFunc.at(name))(this);
			} else if (instruction == "SetSize" && LuaFunc.at("SetSize")) {
				string arg(exec.substr(firstBracketPos, LastBracketPos));
				arg.erase(0, 1);
				arg.erase(arg.size() - 1, arg.size());

				int comma(count(arg.begin(), arg.end(), ','));	//zniczenie przecinkow powinien byc jeden
				if (comma == 1) {
					comma = arg.find(',');
					string firstStringArg(arg.substr(0, comma));
					string secondStringArg(arg.substr(comma + 1, arg.size() - comma));

					int firstArg(0);
					int secArg(0);

					//TODO: tutaj skonczone zle funkcje uzyc atoi albo sscanf
					sprintf_s((char *)firstStringArg.c_str(), firstStringArg.size(), "%d", firstArg);
					sprintf_s((char *)secondStringArg.c_str(), secondStringArg.size(), "%d", secArg);

					sf::Vector2u wektor(firstArg, secArg);
					(*LuaFunc.at(instruction))(this, wektor);
				} else {
					cout << "Zla ilosc argumentow albo brak przecinka miedzy nimi" << endl;
				}
			}
		}
	} catch (luabridge::LuaException const& e) {
		std::cout << "LuaException: " << e.what() << std::endl;
	} catch (exception e) {
		std::cout << "niema takiego skryptu w pliku " << script << std::endl;
	}
}

void Game::LoadTexture() {
	ManadzerTexture::Create();
	ManadzerTexture* p = ManadzerTexture::GetManadzer();

	p->AddTexture("HumanBuldings", "Texture/HumanBuldings.png");
	p->AddTexture("Peasant", "Texture/Peasant.png");
	p->AddTexture("mage", "Texture/mage.png");
	p->AddTexture("GoldMine", "Texture/GoldMine.png");
	p->AddTexture("Icons", "Texture/icons.png");
	p->AddTexture("Base", "Texture/HumanBuldings.png");
}

void Game::SetWindowSize(sf::Vector2u & ref) {
	v_window.setSize(ref);
}

void Game::printListofGameScripts() {
	if (!v_EditorWindow) {
		//wypisywnie w konsoli
		it_map_LuaFunc = this->LuaFunc.begin();
		while (it_map_LuaFunc != LuaFunc.end()) {
			cout << it_map_LuaFunc->first << " " << endl;
			++it_map_LuaFunc;
		}
	} else {
		//dodawanie do okna edytora

		for (it_map_LuaFunc = LuaFunc.begin(); it_map_LuaFunc != LuaFunc.end(); ++it_map_LuaFunc) {
			v_EditorWindow->addTextToConsole("\"" + it_map_LuaFunc->first + "\", ");
		}
	}
}

void Game::printListOfGameObiects() {
	//if (!v_EditorWindow) {
		//wypisywnie w konsoli
	it_map_obiect = Paterns.begin();
	while (it_map_obiect != Paterns.end()) {
		cout << it_map_obiect->second->GetName() << ", ";
		++it_map_obiect;
	}cout << endl;
	//} else {
		//dodawanie do okna edytora
	it_map_obiect = Paterns.begin();
	string names;
	while (it_map_obiect != Paterns.end()) {
		//names += (*it_vec_obiect)->GetName() + ", ";
		v_EditorWindow->addTextToConsole(it_map_obiect->second->GetName() + ", ");
		++it_map_obiect;
	}
	v_EditorWindow->addTextToConsole(names);
	//}
}

void Game::addTextToConsole(const string & arg) {
	if (v_EditorWindow)
		v_EditorWindow->addTextToConsole(arg);
}

bool Game::isCollision(Obiect & ref, sf::RectangleShape & selection) {
	sf::Vector2f obiPos(ref.Getcircle().getPosition().x, ref.Getcircle().getPosition().y);
	float rect = ref.Getcircle().getRadius();
	if (obiPos.x + rect < selection.getPosition().x ||
		obiPos.x - rect > selection.getPosition().x + selection.getSize().x ||
		obiPos.y + rect < selection.getPosition().y ||
		obiPos.y - rect  > selection.getPosition().y + selection.getSize().y) {
		return false;
	}
	return true;
}

void Game::InitPlayers() {
	getGlobalNamespace(L)
		.beginClass<Player>("Player")
		.addProperty("v_id", &Player::GetId)
		.addProperty("GetName", &Player::GetName)
		.addProperty("GetGold", &Player::GetGold)
		.addProperty("GetMetal", &Player::GetMetal)
		.addProperty("GetPopulation", &Player::GetPopulation)
		.addProperty("GetMaxPopulation", &Player::GetMaxPopulation)
		.addProperty("GetAvailablePopulation", &Player::GetAvailablePopulation)
		.addFunction("AddGold", &Player::AddGold)
		.addFunction("AddMetal", &Player::AddMetal)
		.addFunction("AddPop", &Player::AddPop)
		.addFunction("AddPoints", &Player::AddPoints)
		.addFunction("AddToPopulation", &Player::AddToPopulation)
		.addFunction("AddToAvailablePopulation", &Player::AddToAvailablePopulation)
		.endClass();

	v_vec_Players.clear();

	string pName("Player");
	Player *player = new Player(pName, 1000, 500, 0, 0);
	v_vec_Players.push_back(*player);

	pName = "Computer1";
	player = new Player(pName, 800, 300, 0, 0);
	v_vec_Players.push_back(*player);

	pName = "Neutral";
	player = new Player(pName, 0, 0, 0, 0);
	v_vec_Players.push_back(*player);

	cout << "Players:" << endl;
	for (vector<Player>::iterator it = v_vec_Players.begin(); it != v_vec_Players.end(); ++it) {
		cout << "Id " << it->GetId() << " name " << it->GetName() << " gold " << it->GetGold() << " metal " << it->GetMetal() << " population " << it->GetPopulation() << " availablePop " << it->GetAvailablePopulation() << " maxPop " << it->GetMaxPopulation() << endl;
	}
}

void Game::InitEditor() {
	v_EditorWindow = new Editor(&v_vec_Obiects, &Paterns);

	v_interface.GetObiectToView().LoadSettings();

	//ustawienie paneli do edytowania, oraz gracza
	v_interface.SetPlayerToDraw(&(*v_vec_Players.begin()));
	v_EditorWindow->SetPanelToEdit(v_interface);

	v_EditorWindow->setButtonManadzer(&v_m_ButtonManager);
}
void Game::saveConfiguration() {
	//zapis  szablonow przyciskow
	fstream file;
	file.open(v_s_FileButtonRectType, ios::out);
	if (file.is_open()) {
		file << "x y sizeX sizeY";
		sf::IntRect rect(0, 0, 0, 0);
		for (map<string, Button*>::iterator it = v_m_ButtonManager.begin(); it != v_m_ButtonManager.end(); ++it) {
			//pobranie danych do zapisu
			rect = it->second->getTextureRect();
			//zapis danych do pliku
			file << endl << rect.left << " " << rect.top << " " << rect.width << " " << rect.height << " " << it->first;
		}
		file.close();
	}

	for (map<string, Obiect*>::iterator it = Paterns.begin(); it != Paterns.end(); ++it) {
		Obiect* obi = it->second;
		file.open("filesTxt/" + obi->GetName() + ".txt", ios::out);
		if (file.is_open()) {
			//ikona
			sf::IntRect rect(obi->GetSriteIcon().getTextureRect());
			file << "IconRect : " << rect.left << " " << rect.top << " " << rect.width << " " << rect.height;

			if (!it->second->GetIsBuilding()) {	//klatki animacji jednostki
				file << endl << "Animajca : ";
				for (int i = 0; i < direction.m_length; ++i) {
					for (int j = 0; j < it->second->GetFrameAnimationTable()[direction[i]]->m_length; ++j) {
						sf::IntRect rect((*it->second->GetFrameAnimationTable()[i])[j]);
						file << endl << ObiectNameSpace::ObiectDirectionToString(direction[i]) << " : " << rect.left << " " << rect.top << " " << rect.width << " " << rect.height;
					}
				}
				file << endl << "MoveFrameSpeed : " << it->second->GetMoveAnimationSpeed();

				file << endl << "Walka : ";
				for (int i = 0; i < direction.m_length; ++i) {
					for (int j = 0; j < it->second->GetFrameAttackTable()[direction[i]]->m_length; ++j) {
						sf::IntRect rect((*it->second->GetFrameAttackTable()[i])[j]);
						file << endl << ObiectNameSpace::ObiectDirectionToString(direction[i]) << " : " << rect.left << " " << rect.top << " " << rect.width << " " << rect.height;
					}
				}

				file << endl << "Smierc : ";
				for (int i = 0; i < direction.m_length; ++i) {
					for (int j = 0; j < it->second->GetFrameDeathTable()[direction[i]]->m_length; ++j) {
						sf::IntRect rect((*it->second->GetFrameDeathTable()[i])[j]);
						file << endl << ObiectNameSpace::ObiectDirectionToString(direction[i]) << " : " << rect.left << " " << rect.top << " " << rect.width << " " << rect.height;
					}
				}
				file << endl << "TexturePath : " << it->second->GetSpritePath();
			} else {	//sprite budynku
				sf::IntRect rect(it->second->GetSprite().getTextureRect());
				file << endl << "BuildingTextureRect : " << rect.left << " " << rect.top << " " << rect.width << " " << rect.height;

			}
			//TODO: zapis sciezki textury do pliku
			file << endl << "TexturePath : " << it->second->GetSpritePath();
			file.close();
		} else {
			string error("nie udlao sie ptworzyc pliku o nazwie " + obi->GetName() + ".txt");
			cout << error << endl;
			throw exception(error.c_str());
		}
	}

	//zapis podstawowoych danych o grze , uwaga stosuje do niego wzor obiektu
	file.open("filesTxt/Game.txt", ios::out);
	if (file.is_open()) {
		Obiect* obi = Paterns.begin()->second;
		//pozycja ikon jednostki
		file << "IconPosition : " << obi->GetSriteIcon().getPosition().x << " " << obi->GetSriteIcon().getPosition().y;
		file << endl << "IconScale : " << obi->GetSriteIcon().getScale().x << " " << obi->GetSriteIcon().getScale().y;
		file << endl << "NumberOfAnimation : " << v_numberOfAnimationInBuild;
		file.close();
	} else {
		string error("nie udlao sie ptworzyc pliku o nazwie Game.txt");
		cout << error << endl;
		throw exception(error.c_str());
	}

	v_interface.GetObiectToView().SaveSettings();
}

LinesMaps& Game::GetLinesMaps() {
	return linesMaps;
}

void Game::Menu() {
	ImGui::SFML::Init(v_window);

	sf::CircleShape circle(0.0f);
	sf::Clock deltaClock;
	float color[3] = { 0.f, 0.f, 0.f };
	sf::Color bgColor;
	std::string loadScriptPath;

	static Table<bool> selected(map_MapMenager.size(), false);
	selected[0] = true;
	it_selectedMap = map_MapMenager.begin();

	while (v_window.isOpen()) {
		sf::Event event;
		while (v_window.pollEvent(event)) {
			ImGui::SFML::ProcessEvent(event);
			if (event.type == sf::Event::Closed) {
				StatusGry = EXIT;
				v_window.close();
			}

		}
		ImGui::SFML::Update(v_window, deltaClock.restart());

		ImGui::Begin("Menu");

		//TODO: ilosc graczy
		//TODO: nazwy graczy?

		ImGui::SetWindowPos(ImVec2(0, 0));
		ImGui::SetWindowSize(ImVec2(v_window.getSize()));

		ImGui::BeginGroup();
		ImGui::Text("Mapy:");
		ImGui::BeginChild("mapy", ImVec2(v_window.getSize().x*0.5, v_window.getSize().y*0.25), true);

		int i = 0;
		for (it_maps = map_MapMenager.begin(); it_maps != map_MapMenager.end(); ++it_maps) {
			if (ImGui::Selectable(it_maps->second.c_str(), &selected[i])) {
				for (size_t j = 0; j < selected.m_length; ++j) {
					if (i != j)
						selected[j] = false;
				}
				it_selectedMap = it_maps;
			}
			i++;
		}
		ImGui::EndChild();
		ImGui::EndGroup();

		ImGui::BeginGroup();
		ImGui::Text("Scripts:");
		ImGui::BeginChild("scripts", ImVec2(v_window.getSize().x*0.5, v_window.getSize().y*0.25), true);

		for (it_scripts = map_scriptMenager.begin(); it_scripts != map_scriptMenager.end(); ++it_scripts) {
			ImGui::Text((char*)it_scripts->second.c_str());
		}
		ImGui::EndChild();
		ImGui::EndGroup();

		if (ImGui::Button("Play")) { StatusGry = PLAY; return; } ImGui::SameLine();
		if (ImGui::Button("Restart")) { StatusGry = RESTART; ImGui::End(); return; } ImGui::SameLine();
		if (ImGui::Button("Exit")) { StatusGry = EXIT; ImGui::End(); return; }ImGui::SameLine();

		ImGui::End(); // end v_window

		v_window.clear(bgColor); // fill background with color
		v_window.draw(circle);
		ImGui::Render();
		v_window.display();
	}
}

Obiect & Game::FindClosestObiect(string ObiectToFind, Obiect & ref) {
	return Obiect();
}

void Game::StartAttacking(Message arg) {
}
void Game::ProvidedResources(Message arg) {
	//Sprawdzenie jaki to rodzaj materia�u
	//dodanie punktow do gracza
	//dodanie zasobu graczowi
	//Ustawienie obiektowi trase do kopalni w ktorej kopal
}
void Game::EndDigging(Message arg) {
}
void Game::CreateObject(Message arg) {

}
void Game::KillObject(Message arg) {
	arg.p_odbiect->Setlife(0);
	//TODO: macierz przechowujaca objekty martwe, aby moc je pozniej wskrzesic, na razie bedzie usuwany
	vector<Obiect>::iterator p = find(v_vec_Obiects.begin(), v_vec_Obiects.end(), *arg.p_odbiect);
	v_vec_Obiects.erase(p);
}

void Game::InitSoundManager() {
	SoundManager::Create();
	SoundManager* obi = SoundManager::GetManadzer();
	(!obi->AddSound("PeasantSelected1", "Audio/Humans/Peasant/selected1.wav")) ? cout << "Brak pliku dzwiekowego z Peasant1" << endl : cout << "Udalo sie zaladowac pliki z dzwiekami" << endl;
	(!obi->AddSound("PeasantSelected2", "Audio/Humans/Peasant/selected2.wav")) ? cout << "Brak pliku dzwiekowego z Peasant2" << endl : cout << "Udalo sie zaladowac pliki z dzwiekami" << endl;
}