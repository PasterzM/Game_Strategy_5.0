#include "Observer.h"

void Observer::SetObserver(Observed * pSub) {
	if (pSub) {
		if (ObserwowanyObiekt)
			ObserwowanyObiekt->UsunObserwatora(this);
		this->ObserwowanyObiekt = pSub;
		pSub->DodajObserwatora(this);
	}
}

Observer::~Observer() {
	this->ObserwowanyObiekt = NULL;
}