#ifndef MESSAGEMANAGER_H_
#define MESSAGEMANAGER_H_
#include <map>
#include <ostream>

#include "Message.h"

using namespace std;

class Game;

typedef void (Game::*MetodyGame)(Message arg);

class MessageManager {
public:
	static void Create(luabridge::lua_State* L);
	static MessageManager* GetObject();
	static void Destroy();
	MessageManager();
	const map<string, MetodyGame>& GetMesssageType();

	void ExecuteMessage();
	void AddMessageType(string, MetodyGame fun = NULL);
	void AddMessage(Message mess);

	const vector<Message>& GetMessages() const;
private:
	MessageManager(luabridge::lua_State* L);
	static MessageManager* p_MessageManager;
	map<string, MetodyGame> v_m_Functions;
	vector<Message> v_Messages;
};
#endif