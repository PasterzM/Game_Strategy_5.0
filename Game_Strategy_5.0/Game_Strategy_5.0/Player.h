#ifndef PLAYER_H_
#define PLAYER_H_

#include <string>
#include <LuaBridge.h>

using namespace std;

class Player {
public:

	Player(string& name, int gold = 0, int metal = 0, int actPop = 0, int points = 0);

	 int GetId() const;
	 string GetName() const;
	 int GetGold()const;
	 int GetMetal()const;
	 int GetPopulation()const;
	 int GetMaxPopulation()const;
	 int GetAvailablePopulation()const;

	bool AddGold(const int arg);
	bool AddMetal(const int arg);
	bool AddPop(const int arg);
	bool AddPoints(const int arg);

	bool SetPopulation(const int arg);
	bool SetAvailablePopulation(const int arg);
	bool SetMaxPopulation(const int arg);

	bool AddToPopulation(const int arg);
	bool AddToAvailablePopulation(const int arg);
private:
	static int v_counter;
	string v_name;

	int v_id;

	int v_gold;
	int v_metal;
	int v_population;	//ilosc jednostek gracza
	int v_availablePopulation;	//maksymalna ilosc jednostek jaka w danej chwili moze miec gracz
	int v_maxPopulation;	//maksymalna populacja jaka moze miec gracz w grze

	int v_points;
};

#endif