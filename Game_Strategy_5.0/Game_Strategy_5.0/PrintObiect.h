#pragma once
#include "Obiect.h"


class PrintObiect{
public:
	PrintObiect();
	~PrintObiect();

	void SetObiectToPrint(Obiect& ref );
	void SetFont(sf::Font& font);
	void SetCharacterSize(int size);
	void SetColor(sf::Color& col);
	void draw(sf::RenderWindow& ref);
	void SaveSettings();
	void LoadSettings();
	/*
	void SetId(int id);
	void SetName(string name);
	void SetBuilding(bool arg);
	void SetPlayer(int id);

	void  SetSpeed(float speed);
	void  SetDefence(int def);
	void  SetStrength(int str);
	void  SetLife(int life);
	void  SetMaxLife(int life);
	void  SetMana(int mana);
	void  SetMaxMana(int mana);
	void  SetTimeBuilding(float time);
	void  SetFoodCost(int cost);

	void GetId();
	*/
	sf::Text& GetName();
	sf::Text& GetPlayer();
	sf::Text&  GetLife();
	sf::Text&  GetMana();
	/*
	void GetBuilding();
	

	void  GetSpeed();
	void  GetSpeedMoveAnimation();
	void  GetSpeedDeathAnimation();
	void  GetSpeedAttackAnimation();
	void  GetDefence();
	void  GetStrength();
	
	void  GetTimeBuilding();
	void  GetFoodCost();
	*/
private:
	sf::Font v_font;

	sf::Text  v_id;
	sf::Text v_name;
	sf::Text v_isBuilding;
	sf::Text v_idPlayer;
	sf::Text v_size;
	sf::Text  v_speed;
	sf::Text  v_speedMoveAnimation;
	sf::Text  v_speedDeathAnimation;
	sf::Text  v_speedAttackAnimation;
	sf::Text  v_defence;
	sf::Text  v_strength;
	sf::Text  v_life;
	sf::Text  v_mana;
	sf::Text  v_timeBuilding;
	sf::Text  v_foodCost;
};