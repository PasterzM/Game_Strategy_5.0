#include "Button.h"

Button::Button() : v_name("Button"),v_type(ButtonType::BUILDING) {
}

Button::Button(string name, sf::Texture& texture, ButtonType arg) : v_name(name),v_type(arg) {
	this->setTexture(texture);
}

void Button::SetPressed(bool arg) {
	v_isPressed = arg;
}

bool Button::isPressed(sf::RectangleShape & arg) {
	sf::Vector2f pos(getPosition());
	sf::IntRect size(this->getTextureRect());

	if (pos.x > arg.getPosition().x + arg.getSize().x ||
		pos.x < arg.getPosition().x ||
		pos.y > arg.getPosition().y + arg.getSize().y ||
		pos.y < arg.getPosition().y)
		return false;
	else
		return true;
}

bool Button::isPressed() {
	return v_isPressed;
}

ButtonType & Button::GetType() {
	return v_type;
}

void Button::SetType(ButtonType arg) {
	v_type = arg;
}

void Button::SetType(sf::IntRect & rect, ButtonType arg) {
	v_type = arg;
	setTextureRect(rect);
}
