#ifndef TILE_H_
#define TILE_H_
#include <SFML\Graphics.hpp>
#include "Obiect.h"


	enum TileType {
		NOTHING,
		GRASS,	//trawa
		BLOCKED
	};

	class Tile {
	public:
		Tile() :Wsk(NULL), type(TileType::NOTHING) {};
		TileType type;
		sf::RectangleShape Prostokat;
		Obiect *Wsk;
	};

#endif