#ifndef BUTTON_H_
#define BUTTON_H_
#include <SFML\Graphics.hpp>

using namespace std;

enum ButtonType {
	BUILDING,
	UNIT,
	CAPACITY
};

class Button: public sf::Sprite {
public:
	Button();
	Button(string name, sf::Texture& texture, ButtonType arg);
	void SetPressed(bool arg);
	bool isPressed(sf::RectangleShape &arg);
	bool isPressed();
	ButtonType& GetType();
	void SetType(ButtonType arg);
	void SetType(sf::IntRect& rect,ButtonType arg);
private:
	string v_name;
	bool v_isPressed;
	ButtonType v_type;
	static string v_s_FileButtonName;
};

#endif