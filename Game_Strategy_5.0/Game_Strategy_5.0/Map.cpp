#include <cmath>
#include "Map.h"

//using namespace Plytki;

Map *Map::Obiekt = NULL;
sf::Vector2i Map::WielkoscPlytki(0, 0);

Map::Map() {

}

Map::~Map() {
	for (int i = 0; i < Wysokosc; i++)
		delete[] Plytka[i];
	delete[] Plytka;
	Plytka = NULL;
}


const sf::Vector2i& Map::GetWielkoscPlytki() {
	return Map::WielkoscPlytki;
}


void Map::CreateMap(std::string & filename, const int tileSize) {

	std::fstream file;
	std::string trash;

	file.open(filename, std::ios::in);
	if (!file.is_open())
		cout << " Map::CreateMap //  brak pliku " << endl;

	file >> Dlugosc;
	file >> Wysokosc;

	Plytka = new Tile*[Wysokosc];

	for (int i = 0; i < Wysokosc; i++) Plytka[i] = new Tile[Dlugosc];

	int buffer;

	//TODO: zamienic na kolor z pliku
	sf::Color trawa(0, 33, 0);
	sf::Color woda(0, 0, 0x9c);

	WielkoscPlytki.x = tileSize;
	WielkoscPlytki.y = tileSize;

	//Map
	for (int i = 0; i < Wysokosc; i++) {
		for (int j = 0; j < Dlugosc; j++) {
			sf::Vector2i cord(j, i);
			file >> buffer;


			switch (buffer) {
			case 1:
				Plytka[j][i].type = TileType::GRASS;
				Plytka[j][i].Prostokat.setFillColor(trawa);
				break;
				//blokada
			case 2:
				Plytka[j][i].type = TileType::BLOCKED;
				Plytka[j][i].Prostokat.setFillColor(woda);
				break;
			default:
				std::cout << "Wrong type of obiect in the file on position : " << j << ", " << i << std::endl;
				break;
			}
			Plytka[j][i].Prostokat.setPosition(sf::Vector2f(cord.x*WielkoscPlytki.x, cord.y*WielkoscPlytki.y));
			Plytka[j][i].Prostokat.setSize(sf::Vector2f(tileSize, tileSize));

		}
	}
	//obejscie niepotrzebnych linijek
	getline(file, trash);
	getline(file, trash);
	getline(file, trash);
}

void Map::Stworz() {
	if (!Obiekt)
		Obiekt = new Map;
}

void Map::Zniszcz() {
	if (Obiekt != NULL)
		delete Obiekt;
	Obiekt = NULL;
}

Map& Map::GetObiekt() {
	return *(Map::Obiekt);
}

void Map::showMap() {
	for (int i = 0; i < Wysokosc; i++) {
		for (int j = 0; j < Dlugosc; j++) {
			cout << Plytka[j][i].type << " ";
		}
		cout << endl;
	}
}

void Map::showWskMap() {
	for (int i = 0; i < Wysokosc; i++) {
		for (int j = 0; j < Dlugosc; j++) {
			if (Plytka[j][i].Wsk)
				cout << "1 ";
			else
				cout << "0 ";
		}
		cout << endl;
	}
}

void Map::Rysuj(sf::RenderWindow & window) {
	for (int i = 0; i < Wysokosc; i++) {
		for (int j = 0; j < Dlugosc; j++) {
			window.draw(Plytka[j][i].Prostokat);
		}
	}
}

const int & Map::GetDlugosc() {
	return Dlugosc;
}

const int & Map::GetWysokosc() {
	return Wysokosc;
}

Tile& Map::getPlytka(int x, int y) {
	return Plytka[x][y];
}

Tile& Map::getPlytka(sf::Vector2f & ref) {
	int x = std::floor(ref.x);
	int y = std::floor(ref.y);
	return Plytka[x][y];
}

Tile & Map::getPlytka(sf::Vector2i & ref) {
	return Plytka[ref.x][ref.y];
}
