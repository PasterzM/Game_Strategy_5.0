#include "TextureManager.h"
#include "Functions.h"

ManadzerTexture* ManadzerTexture::v_s_Obiect = NULL;
string ManadzerTexture::v_s_TexturePath = "Texture";

ManadzerTexture * ManadzerTexture::GetManadzer() {
	if (v_s_Obiect)
		return v_s_Obiect;
	return nullptr;
}

void ManadzerTexture::Create() {
	if (v_s_Obiect) {
		delete v_s_Obiect;
		v_s_Obiect = new ManadzerTexture;
	} else {
		v_s_Obiect = new ManadzerTexture;
	}
}

void ManadzerTexture::Remove() {
	if (v_s_Obiect) {
		for_each(v_s_Obiect->GetMap().begin(), v_s_Obiect->GetMap().end(), deletesMap<string, sf::Texture*>);
		v_s_Obiect->GetMap().clear();
		delete v_s_Obiect;
		v_s_Obiect = NULL;
	}
}

bool ManadzerTexture::AddTexture(string  name, string  path) {
	sf::Texture texture;
	if (texture.loadFromFile(path)) {
		v_m_texute[name] = new sf::Texture(texture);
		return true;
	}
	return false;
}

map<std::string, sf::Texture*>& ManadzerTexture::GetMap() {
	return v_m_texute;
}

ManadzerTexture::~ManadzerTexture() {
	for_each(v_m_texute.begin(),v_m_texute.end(),deletesMap<string,sf::Texture*>);
	v_m_texute.clear();
}
