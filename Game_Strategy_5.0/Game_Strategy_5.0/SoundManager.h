#ifndef SOUNDMANAGER_H_
#define SOUNDMANAGER_H_
#include <map>
#include <SFML\Audio.hpp>
#include <iterator>

using namespace std;

class SoundManager {
public:
	static void Create();
	static void Destroy();

	bool AddSound(std::string name,std::string path);
	static SoundManager* GetManadzer();

	sf::Sound* GetSound(string);

	map<string, sf::Sound*>& GetMap();
private:
	SoundManager();
	~SoundManager();
	static SoundManager* p_obiect;
	map<string,sf::SoundBuffer*> v_m_SoundBuffers;
	map<string,sf::Sound*> v_m_Sounds;
};

#endif