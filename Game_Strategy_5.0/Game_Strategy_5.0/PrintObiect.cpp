#include "PrintObiect.h"
#include <iostream>
#include <fstream>

using namespace std;

PrintObiect::PrintObiect() {
	if (!v_font.loadFromFile("Fonts/arial.ttf"))
		cout << "Nie udalo sie zaladowac plikow z czcionka" << endl;
	else
		cout << "udalo sie zaladowac plikow z czcionka" << endl;

	v_id.setFont(v_font);
	v_name.setFont(v_font);
	v_isBuilding.setFont(v_font);
	v_idPlayer.setFont(v_font);
	v_size.setFont(v_font);
	v_speed.setFont(v_font);
	v_speedMoveAnimation.setFont(v_font);
	v_speedDeathAnimation.setFont(v_font);
	v_speedAttackAnimation.setFont(v_font);
	v_defence.setFont(v_font);
	v_strength.setFont(v_font);
	v_life.setFont(v_font);
	v_mana.setFont(v_font);
	v_timeBuilding.setFont(v_font);
	v_foodCost.setFont(v_font);

	sf::Color color(0xff, 0xff, 0xff, 0xff);

	v_id.setColor(color);
	v_name.setColor(color);
	v_isBuilding.setColor(color);
	v_idPlayer.setColor(color);
	v_size.setColor(color);
	v_speed.setColor(color);
	v_speedMoveAnimation.setColor(color);
	v_speedDeathAnimation.setColor(color);
	v_speedAttackAnimation.setColor(color);
	v_defence.setColor(color);
	v_strength.setColor(color);
	v_life.setColor(color);
	v_mana.setColor(color);
	v_timeBuilding.setColor(color);
	v_foodCost.setColor(color);

	LoadSettings();
}

PrintObiect::~PrintObiect() {
#ifdef _DEBUG
	SaveSettings();
#endif // _DEBUG
}

void PrintObiect::SetObiectToPrint(Obiect & ref) {
	stringstream ss;

	ss.str("");
	ss << ref.GetName();
	v_name.setString(ss.str());

	ss.str("");
	ss << ref.Getlife() << "/" << ref.GetmaxLife();
	v_life.setString(ss.str());

	ss.str("");
	ss << ref.Getmana() << "/" << ref.GetmaxMana();
	v_mana.setString(ss.str());

	ss.str("");
	ss << ref.GetPlayer().GetName();
	v_idPlayer.setString(ss.str());
}

void PrintObiect::SetFont(sf::Font& font) {
	v_id.setFont(font);
	v_name.setFont(font);
	v_isBuilding.setFont(font);
	v_idPlayer.setFont(font);
	v_size.setFont(font);
	v_speed.setFont(font);
	v_speedMoveAnimation.setFont(font);
	v_speedDeathAnimation.setFont(font);
	v_speedAttackAnimation.setFont(font);
	v_defence.setFont(font);
	v_strength.setFont(font);
	v_life.setFont(font);
	v_mana.setFont(font);
	v_timeBuilding.setFont(font);
	v_foodCost.setFont(font);
}

void PrintObiect::SetCharacterSize(int size) {
	v_id.setCharacterSize(size);
	v_name.setCharacterSize(size);
	v_isBuilding.setCharacterSize(size);
	v_idPlayer.setCharacterSize(size);
	v_size.setCharacterSize(size);
	v_speed.setCharacterSize(size);
	v_speedMoveAnimation.setCharacterSize(size);
	v_speedDeathAnimation.setCharacterSize(size);
	v_speedAttackAnimation.setCharacterSize(size);
	v_defence.setCharacterSize(size);
	v_strength.setCharacterSize(size);
	v_life.setCharacterSize(size);
	v_mana.setCharacterSize(size);
	v_timeBuilding.setCharacterSize(size);
	v_foodCost.setCharacterSize(size);
}

void PrintObiect::SetColor(sf::Color & col) {
	v_id.setColor(col);
	v_name.setColor(col);
	v_isBuilding.setColor(col);
	v_idPlayer.setColor(col);
	v_size.setColor(col);
	v_speed.setColor(col);
	v_speedMoveAnimation.setColor(col);
	v_speedDeathAnimation.setColor(col);
	v_speedAttackAnimation.setColor(col);
	v_defence.setColor(col);
	v_strength.setColor(col);
	v_life.setColor(col);
	v_mana.setColor(col);
	v_timeBuilding.setColor(col);
	v_foodCost.setColor(col);
}

void PrintObiect::draw(sf::RenderWindow & ref) {
	ref.draw(v_name);
	ref.draw(v_idPlayer);
	ref.draw(v_life);
	ref.draw(v_mana);
}

void PrintObiect::SaveSettings() {
	fstream file("Fonts/Strings position.txt", ios::out);

	if (file.is_open()) {

		file << "name " << v_name.getPosition().x << " " << v_name.getPosition().y << " " << v_name.getCharacterSize();
		file << endl << "life " << v_life.getPosition().x << " " << v_life.getPosition().y << " " << v_life.getCharacterSize();
		file << endl << "mana " << v_mana.getPosition().x << " " << v_mana.getPosition().y << " " << v_mana.getCharacterSize();
		file << endl << "player " << v_idPlayer.getPosition().x << " " << v_idPlayer.getPosition().y << " " << v_idPlayer.getCharacterSize();

		file.close();
	} else {
		cout << "Obiekt PrintObiect nie mogl zapisac pozycji napisow poniewaz nie udalo mu sie otworzyc pliku" << endl;
	}
}

void PrintObiect::LoadSettings() {
	fstream file("Fonts/Strings position.txt", ios::in);

	if (file.is_open()) {
		string trash;
		sf::Vector3f pos;

		file >> trash >> pos.x >> pos.y >> pos.z; v_name.setPosition(pos.x, pos.y); v_name.setCharacterSize(pos.z);
		file >> trash >> pos.x >> pos.y >> pos.z; v_life.setPosition(pos.x, pos.y); v_life.setCharacterSize(pos.z);
		file >> trash >> pos.x >> pos.y >> pos.z; v_mana.setPosition(pos.x, pos.y); v_mana.setCharacterSize(pos.z);
		file >> trash >> pos.x >> pos.y >> pos.z; v_idPlayer.setPosition(pos.x, pos.y); v_idPlayer.setCharacterSize(pos.z);

		file.close();

	} else {
		cout << "Obiekt PrintObiect nie mogl wczytac pozycji napisow poniewaz nie udalo mu sie otworzyc pliku" << endl;
	}
}

sf::Text& PrintObiect::GetName() {
	return v_name;
}

sf::Text & PrintObiect::GetPlayer() {
	return v_idPlayer;
}

sf::Text & PrintObiect::GetLife() {
	return v_life;
}

sf::Text & PrintObiect::GetMana() {
	return v_mana;
}
