#ifndef OBIECT_H_
#define OBIECT_H_
#include <string>
#include <LuaBridge.h>
#include <memory>
#include <SFML/Graphics.hpp>
#include "Player.h"
#include "Observed.h"
#include "Observer.h"
#include "Table.h"

using namespace std;

namespace ObiectNameSpace {

	enum ObiectMode {
		NOTHING,
		WAITING,
		BUILD,
		CREATING,
		MOVING,
		ATTACKING,
		DIGGING,
		APPLY	//odnoszenie 
	};
	//kierunki swiata
	enum ObiectDirection {
		STOP,
		N,
		NE,
		E,
		SE,
		S,
		WS,
		W,
		NW,
	};

	enum ObiectMineType {
		EMPTY,
		GOLD,
		METAL
	};

	enum ObiectType {
		PROTOTYPE,
		OBIECT
	};

	string ObiectDirectionToString(ObiectDirection arg);
	ObiectDirection StringToObiectDirection(string arg);
};


class Obiect : public Observed, public Observer {
public:
	Obiect() :v_id(0) {};
	Obiect(const Obiect* prototype, float posX, float posY, Player& player);	//tworzenie konkretnych obiektow
	Obiect(const string& name, const  string& fileName);	//tworzenie szablonow

	~Obiect();

	static void RegisterClassObiect(luabridge::lua_State * L);
	void draw(sf::RenderWindow& window);

	sf::Sprite& GetSriteIcon();
	sf::Sprite& GetSprite();
	string GetName() const;
	sf::Sprite Getsprite() const;
	sf::Texture Gettexture() const;
	sf::CircleShape Getcircle() const;
	float Getspeed() const;
	float GetspeedMoveAnimation() const;
	float GetspeedDeathAnimation() const;
	float GetspeedAttackAnimation() const;
	int Getdefence() const;
	int Getstrength() const;
	int Getlife() const;
	int GetmaxLife() const;
	int Getmana() const;
	int GetmaxMana() const;
	float GettimeBuilding() const;
	int GetfoodCost() const;
	bool GetIsBuilding()const;
	int GetSize() const;
	int GetColor()const;
	const sf::Vector2f& GetPosition()const;
	const int& GetId();
	Player& GetPlayer()const;
	Obiect& GetObserver() const;
	string& GetTransportingType() const;


	void Setsprite(sf::Sprite arg);
	void SetSprite(sf::Texture& arg);
	void setRadius(float rect);

	void SetName(string arg);
	void SetTexture(sf::Texture arg);
	void Setcircle(sf::CircleShape arg);
	void Setspeed(float arg);
	void SetspeedMoveAnimation(float arg);
	void SetspeedDeathAnimation(float arg);
	void SetspeedAttackAnimation(float arg);
	void Setdefence(int arg);
	void Setstrength(int arg);
	void Setlife(int arg);
	void SetmaxLife(int arg);
	void Setmana(int arg);
	void SetmaxMana(int arg);
	void SettimeBuilding(float arg);
	void SetfoodCost(int arg);
	void SetSize(int arg);
	void SetColor(int *arg);
	void SetMode(string mode);
	void SetTransporingType(string arg);

	sf::Texture& GetTexture();

	void printData();
	bool LoadScript(string scriptName);
	bool executeScript(string scriptName);

	void SetIsBuilding(bool is);

	void SetPosition(float x, float y);
	void Move(float x, float y);

	bool LoadScriptfunctionFromWindow(string tab);
	void setSelected(bool arg);

	const sf::IntRect& GetTextureRect();

	void SetPath(vector<sf::Vector2i>& path);

	void Update(sf::Time& dt);

	void AktualizujObiektObserwowany();

	void DrawInterface(sf::RenderWindow &ref);

	void AddToListOfButtons(string name);
	vector<string>& GetListOfButtons();

	Table<Table<sf::IntRect>*>& GetFrameAnimationTable();
	Table<Table<sf::IntRect>*>& GetFrameAttackTable();
	Table<Table<sf::IntRect>*>& GetFrameDeathTable();

	ObiectNameSpace::ObiectDirection& GetDirection();
	float& GetMoveAnimationSpeed();
	void SetMoveAnimationSpeed(float speed);

	void SetSpritePath(const string& arg);
	string& GetSpritePath();

	bool operator==(const Obiect& ref);
	
	string GetModeToString() const;
	ObiectNameSpace::ObiectMode GetStringToMode(string mode);
private:
	void SetModeByType(ObiectNameSpace::ObiectMode mode);
	static int v_count;
	int v_id;
	ObiectNameSpace::ObiectType v_ObiectType;

	string v_name;
	bool v_isBuilding;
	Player* v_idPlayer;

	sf::Sprite v_spriteIcon;
	sf::Sprite v_sprite;
	sf::Texture v_texture;
	sf::CircleShape v_circle;

	int v_size;
	float v_speed;
	float v_speedMoveAnimation;
	float v_speedDeathAnimation;
	float v_speedAttackAnimation;
	int v_defence;
	int v_strength;
	int v_life;
	int v_maxLife;
	int v_mana;
	int v_maxMana;
	float v_timeBuilding;
	int v_foodCost;
	int v_numOfButtons;
	vector<string> v_VecOfButtons;

	bool v_isSelected;

	void move(sf::Vector2f& arg);
	void setPosition(sf::Vector2f& arg);
	void setPosition(float x, float y);
	sf::Vector2f ObliczWektorKierunkowyJednostki(sf::Vector2f & start, sf::Vector2f & koniec);
	sf::Vector2i nastPoz;
	sf::Vector2f WektorKierunkowy;

	static luabridge::lua_State *L;
	map<string, std::shared_ptr<luabridge::LuaRef>> LuaFunc;
	bool LoadScript(const char* scriptFilename, const std::string& tableName);
	const string *v_fileName;

	vector<sf::Vector2i> *v_vec_path;

	ObiectNameSpace::ObiectMode v_mode;
	void move(sf::Time &dt);
	void attack(sf::Time& dt);

	bool LoadScriptFunction(string name);

	Table<Table<sf::IntRect>*>* p_animationFrames;
	Table<Table<sf::IntRect>*>* p_attackFrames;
	Table<Table<sf::IntRect>*>* p_deathFrames;

	float v_actualAnimationTime;
	ObiectNameSpace::ObiectDirection v_direction;
	
	int v_actualframe;
	int v_actualNumberOfHits;	//ilosc uderzen wykonanych do tej pory
	int v_numberOfHitsToDigout;	//ilosc uderzen potrzebnych do wykopania

	ObiectNameSpace::ObiectDirection CalculateDirection(sf::Vector2f& start, sf::Vector2f& end);

	string v_texturePath;

	ObiectNameSpace::ObiectMineType v_typeOfTransportingMineral;

	void dig(sf::Time& dt);
	void apply(sf::Time& dt);
};

#endif // !CHARACTER_H_