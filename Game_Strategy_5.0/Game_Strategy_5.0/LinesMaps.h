#ifndef LINESMAPS_H_
#define LINESMAPS_H_
#include <SFML\Graphics.hpp>
#include "Map.h"

class LinesMaps: public sf::Drawable {
	public:
		LinesMaps() {};
		LinesMaps(Map& map);
		~LinesMaps();
		void SetViewLines(bool arg);
		bool& GetWidocznoscLinij();
		void SetMaps(Map& map);
	private:
		sf::RectangleShape** Prostokaty;
		bool isLinieMapy;
		int v_height;
		int v_width;

		virtual void draw(sf::RenderTarget &, sf::RenderStates) const;
};

#endif