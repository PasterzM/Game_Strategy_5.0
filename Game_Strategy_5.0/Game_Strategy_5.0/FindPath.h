#ifndef ZNAJDZSCIEZKE_H_
#define ZNAJDZSCIEZKE_H_

#include <vector>
#include <cmath>
#include <algorithm>
#include <SFML\Graphics.hpp>
//#include "MenadzerGlobalnych.h"
#include "Map.h"
//#include "Obiekt.h"
//#include "Budynek.h"
#include "AStarInit.h"

using namespace std;

class FindPath {
public:
	FindPath();
	~FindPath();
	std::vector<sf::Vector2i>* SzukajSciezki(sf::Vector2i & poczatek, sf::Vector2i & cel);
	std::vector<sf::Vector2i>* SzukajSciezki(sf::Vector2i & poczatek, sf::Vector2i & cel, AGwiazdkaInit &init);

	ostream& WypiszSciezke(ostream &str, sf::Vector2i & from, sf::Vector2i & destination);

	void PokazMape(sf::Vector2i & poczatek, sf::Vector2i & cel);

	void SetMap(Map &map);
private:
	//std::vector<Obiekt*> *WskObiekt;

	Map *mapa;

	TileType **ZamkSciezki;

	void init();

	sf::Vector2i ZnajdzMini(std::vector<sf::Vector2i> tab, sf::Vector2i &cel);
};
#endif