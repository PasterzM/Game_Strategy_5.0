#include <iostream>
#include <fstream>
#include "Interface.h"
#include "TextureManager.h"

string Interface::v_s_fileButtonsName("filesTxt/Button position.txt");

Interface::Interface() : v_isPanelView(true), v_pButton(ButtonsCounter()), p_obiectToView(NULL), p_player(NULL) {
	InitButtonsPosition();
}

Interface::Interface(int numberOfButtons, bool isPanel) : v_isPanelView(isPanel), v_pButton(numberOfButtons), p_obiectToView(NULL) {
	InitButtonsPosition();
}

Interface::~Interface() {
#ifdef _DEBUG
	//trzeba zalozyc ze sa jakies przyciski
	fstream file(v_s_fileButtonsName, ios::out);
	if (file.is_open()) {
		file << "x y sizeX sizeY";
		sf::Vector2f buttonPos;
		sf::Vector2f scale;
		for (int i = 0; i < v_pButton.m_length; ++i) {
			//pobranie danych do zapisu
			buttonPos = v_pButton[i].getPosition();
			scale = v_pButton[i].getScale();
			//zapis danych do pliku
			file << endl << buttonPos.x << " " << buttonPos.y << " " << scale.x << " " << scale.y;
		}
		file.close();
	}
#endif
}

bool Interface::isPanelView() {
	return v_isPanelView;
}

void Interface::SetIsPanelView(bool arg) {
	v_isPanelView = arg;
}

sf::View & Interface::GetGameView() {
	return v_gameView;
}

sf::View & Interface::GetPanelView() {
	return v_panelView;
}

sf::View & Interface::GetMiniMapView() {
	return v_miniMap;
}

void Interface::Draw(sf::RenderWindow & win) {
	for (int i = 0; i < v_pButton.m_length; i++) {
		win.draw(v_pButton[i]);
	}
	v_printObi.draw(win);
}

void Interface::DrawPlayer(sf::RenderWindow & win) {
	if (p_player) {
		win.draw(v_playerGold);
		win.draw(v_playerMetal);
		win.draw(v_playerFood);
	}
}

Table<Button>& Interface::GetButtonTable() {
	return v_pButton;
}

void Interface::SetButtonsType(vector<string>& obiButtons, map<string, Button*>& buttonManadzer) {
	vector<string>::iterator it = obiButtons.begin();

	//ustawienia typow przyciskow na interfejsie
	for (int i = 0; i < v_pButton.m_length; ++i) {
		if (it != obiButtons.end() && buttonManadzer.find(*it) != buttonManadzer.end()) {
			v_pButton[i].SetType(buttonManadzer[*it]->GetType());
			v_pButton[i].setTextureRect(buttonManadzer[*it]->getTextureRect());
		} else {
			v_pButton[i].SetType(ButtonType::CAPACITY);
			v_pButton[i].setTextureRect(buttonManadzer["None"]->getTextureRect());
		}
		if (it != obiButtons.end())
			++it;
	}
}

void Interface::SetObiectToView(Obiect * obi) {
	p_obiectToView = obi;
	v_printObi.SetObiectToPrint(*obi);
}

PrintObiect& Interface::GetObiectToView() {
	return v_printObi;
}

void Interface::SetPlayerToDraw(Player* arg) {
	p_player = arg;

	stringstream ss;
	ss << "Gold: " << p_player->GetGold();
	cout << ss.str() << endl;
	v_playerGold.setString(ss.str());
	ss.str("");

	ss << "Metal: " << p_player->GetMetal();
	cout << ss.str() << endl;
	v_playerMetal.setString(ss.str());
	ss.str("");

	ss << "Food: " << p_player->GetPopulation() << "/" << p_player->GetMaxPopulation();
	cout << ss.str() << endl;
	v_playerFood.setString(ss.str());
	ss.str("");

	if (!v_font.loadFromFile("Fonts/arial.ttf"))
		cout << "Nie udalo sie zaladowac plikow z czcionka" << endl;
	else
		cout << "udalo sie zaladowac plikow z czcionka" << endl;

	v_playerGold.setFont(v_font);
	v_playerMetal.setFont(v_font);
	v_playerFood.setFont(v_font);

	sf::Color color(0xff, 0xff, 0xff, 0xff);

	v_playerGold.setColor(color);
	v_playerMetal.setColor(color);
	v_playerFood.setColor(color);

	v_playerGold.setCharacterSize(32);
	v_playerMetal.setCharacterSize(32);
	v_playerFood.setCharacterSize(32);

	//TODO:wczytywanie danych
	//LoadSettings();
}

sf::Text & Interface::GetPlayerGold() {
	return v_playerGold;
}

sf::Text & Interface::GetPlayerMetal() {
	return v_playerMetal;
}

sf::Text & Interface::GetPlayerFood() {
	return v_playerFood;
}

int Interface::ButtonsCounter() {
	int counter = 0;
	fstream file(v_s_fileButtonsName, ios::in);
	if (file.is_open()) {
		string txt;
		while (!file.eof()) {
			getline(file, txt);
			++counter;
		}
		file.close();
	}

	return counter - 1;
}

void Interface::InitButtonsPosition() {
	fstream file(v_s_fileButtonsName, ios::in);

	if (file.is_open()) {
		string smieci;
		getline(file, smieci);

		sf::Vector2f buttonPos(0, 0);
		sf::Vector2f scale(1, 1);

		for (int i = 0; i < v_pButton.m_length; ++i) {
			//wczytanie danych z pliku
			file >> buttonPos.x >> buttonPos.y >> scale.x >> scale.y;

			//zapisanie danych do przyciskow
			v_pButton[i].setPosition(buttonPos);
			v_pButton[i].setScale(scale);
		}
		file.close();
	}
}
