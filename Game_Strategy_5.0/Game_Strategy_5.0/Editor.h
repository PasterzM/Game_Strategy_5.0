#ifndef EDITOR_H_
#define EDITOR_H_
#include <SFML\Graphics.hpp>
#include <SFML\Audio.hpp>
#include <list>
#include "Obiect.h"
#include "Table.h"
#include "Button.h"

class Interface;

class Editor {
public:
	string& GetScript() {
		return toReturn;
	}
	string reset() {
		string lastChange(toReturn);
		toReturn = "";
		return lastChange;
	}
	Editor(vector<Obiect>* vect, map<string, Obiect*>* vecPaterns);
	~Editor();

	const sf::RenderWindow& GetWindow();

	void addTextToConsole(const std::string arg);

	void ExecuteEditor();
	//void clear();
	void Exit();

	void SetObiectToEdit(Obiect* p);
	void ClearSelectedObiect();

	void SetPanelToEdit(Interface& arg);

	void setButtonManadzer(map<string, Button*>* arg);

	void CreateEditorInLua(luabridge::lua_State *L);
private:
	sf::RenderWindow window;

	sf::CircleShape v_circle;

	char v_script[255];
	string toReturn;
	list<string> list_output;

	void fillTableChar(char tab[], char c, int size) {
		for (size_t i = 0; i < size; i++) {
			v_script[i] = c;
		}
	}

	bool v_isPressEnter;
	bool v_isLines;
	bool v_isPanelView;
	vector<Obiect> *p_vecObiect;
	Obiect *p_vecSelectedObiect;

	//map<string, sf::Texture> v_map_Textures;

	Table<bool> *v_selectedTexture;
	Table<bool> *selectedSound;
	sf::Sound *p_selectedSound;

	Table<bool>* p_constructionAnimation;	//animacja butowy obiektu, stosowane przy budowie budynkow
	Table<sf::FloatRect>* p_rectConstuctionAnimation;

	void LoadFiles();

	sf::Texture *v_pSelectedTexture;


	sf::Vector2f v_textureIconPosition;
	sf::IntRect v_textureIconRect;
	sf::Vector2f v_IconScale;
	sf::IntRect v_IconSize;
	sf::FloatRect v_textureIcon;

	int buttonInd;
	sf::Vector2f v_buttonPos;
	sf::Vector2f v_buttonSize;
	sf::IntRect v_buttonRect;

	void SetTextureByObiectTexture(string& name);

	Interface* v_p_interface;

	string v_numberOfButtons;

	//konfiguracja rodzajow przyciskow
	map<string, Button*>* v_p_ButtonManadzer;
	Table<bool>* v_selectedButton;
	Button* v_p_SelectedButton;



	sf::FloatRect v_ButtonTextureRect;
	sf::Vector2f v_scaleButtontextRect;
	int v_buttonType;

	map<string, Obiect*>* v_vecObiectsPaterns;

	//interface, wyswietlany tekst dotyczacy obiektu
	void SetStringsPosition();
	int v_characterSize;

	sf::Vector3f v_namePosition;
	sf::Vector3f v_lifePosition;
	sf::Vector3f v_manaPosition;
	sf::Vector3f v_PlayerPosition;

	//animacje
	void AnimationEditor(string name, int &selectedFrame, int& countFrame, int& indDirection, sf::FloatRect& textureShape, Table<Table<sf::IntRect>*>& table);

	int v_selectedFrameFromAnimation;	//aktualnie modyfikowana klatka animacji
	int v_countFrameInAnimation;	//ilosc klatek w animacji
	int v_indOfDirection;			//indeks kierunku aktualnie modyfikowany
	sf::FloatRect v_textureMoveShape;	//wybrany kawalek textury

	int v_selectedFrameFromAttack;
	int v_countFrameInAttack;
	int v_indOfAttack;
	sf::FloatRect v_textureAttackShape;

	int v_selectedFrameDeath;
	int v_countFrameInDeath;
	int v_indOfDeath;
	sf::FloatRect v_textureDeathShape;

	//koniec animacji



	float v_AnimationSpeed;


	void SetPlayerPosition();
	sf::Vector2f v_playerGoldPosition;
	sf::Vector2f v_playerMetalPosition;
	sf::Vector2f v_playerFoodPosition;


};
#endif