#include "FindPath.h"

FindPath::FindPath() : mapa(&Map::GetObiekt()) {
}

FindPath::~FindPath() {
	//WskObiekt = NULL;
}

std::vector<sf::Vector2i>* FindPath::SzukajSciezki(sf::Vector2i & poczatek, sf::Vector2i & cel) {
	cout << "FIND PATH ------------------------------------------------------" << endl;
	cout << "Poczatek : " << poczatek.x << " " << poczatek.y << endl;
	cout << "cel : " << cel.x << " " << cel.y << endl;
	int dlugosc = mapa->GetDlugosc();
	int wysokosc = mapa->GetWysokosc();

	//inicjalizacja
	ZamkSciezki = new TileType*[wysokosc];
	sf::Vector2i punkt(poczatek);	// punkt wzgledem ktorego algorytm wyszukuje sciezki

	std::vector<sf::Vector2i> neighborpunkt;
	neighborpunkt.reserve(8);
	std::vector<sf::Vector2i> *path;	//kordynaty wynikow
	path = new vector<sf::Vector2i>;

	for (int i = 0; i < wysokosc; i++)	ZamkSciezki[i] = new TileType[dlugosc];	//	macierz zamknietych tras

	for (int i = 0; i < wysokosc; i++) {	//inicjalizacja macierzy zamknietych tras
		for (int j = 0; j < dlugosc; j++) {
			if (mapa->getPlytka(j, i).type == TileType::GRASS)	//wszystko inne nie bedace trawa lub piaskiem jest blokada dla jednostki
				ZamkSciezki[j][i] = GRASS;
			else
				ZamkSciezki[j][i] = TileType::BLOCKED;
		}
	}
	//PokazMape(poczatek, cel);
	/////////////////////////
	//glowna czesc algorytmu
	/////////////////////////
	while (!(punkt.x == cel.x && punkt.y == cel.y)) {
		ZamkSciezki[punkt.x][punkt.y] = BLOCKED;
		path->push_back(punkt);
		//jezeli punkt nie znajduje sie na krawedzi
		if (punkt.x > 0 && punkt.x < dlugosc && punkt.y > 0 && punkt.y < wysokosc) {
			if (ZamkSciezki[punkt.x - 1][punkt.y - 1] != GRASS &&
				ZamkSciezki[punkt.x][punkt.y - 1] != GRASS && ZamkSciezki[punkt.x + 1][punkt.y - 1] != GRASS && ZamkSciezki[punkt.x + 1][punkt.y] != GRASS &&
				ZamkSciezki[punkt.x + 1][punkt.y + 1] != GRASS && ZamkSciezki[punkt.x][punkt.y + 1] != GRASS && ZamkSciezki[punkt.x - 1][punkt.y + 1] != GRASS &&
				ZamkSciezki[punkt.x - 1][punkt.y] != GRASS) {	// wokol punkt nie ma wolnego punktu
				if (path->size() > 0) {	//jezeli moze sie cofnac
					punkt = path->at(path->size() - 1);
					path->pop_back();
				} else {
					std::cout << " There is no path to destination " << endl;
					neighborpunkt.clear();
					for (int i = 0; i < wysokosc; i++)
						delete ZamkSciezki[i];
					delete[] ZamkSciezki;
					return path;
				}
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y));
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y + 1));
			}
			if (ZamkSciezki[punkt.x][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x, punkt.y + 1));
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y + 1));
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y));
			}
			punkt = ZnajdzMini(neighborpunkt, cel);
			neighborpunkt.clear();
		}
		// jezeli punkt znajduje sie przy gornej krawedzi
		else if (punkt.y == 0 && punkt.x > 0 && punkt.x < dlugosc) {
			if (ZamkSciezki[punkt.x + 1][punkt.y] != GRASS &&
				ZamkSciezki[punkt.x + 1][punkt.y + 1] != GRASS && ZamkSciezki[punkt.x][punkt.y + 1] != GRASS && ZamkSciezki[punkt.x - 1][punkt.y + 1] != GRASS &&
				ZamkSciezki[punkt.x - 1][punkt.y] != GRASS) {	// wokol punkt nie ma wolnego punktu
				if (path->size() > 0) {	//jezeli moze sie cofnac
					punkt = path->at(path->size() - 1);
					path->pop_back();
				} else {
					std::cout << " There is no path to destination " << endl;
					neighborpunkt.clear();
					for (int i = 0; i < wysokosc; i++)
						delete ZamkSciezki[i];
					delete[] ZamkSciezki;
					return path;
				}
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y));
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y + 1));
			}
			if (ZamkSciezki[punkt.x][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x, punkt.y + 1));
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y + 1));
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y));
			}
			punkt = ZnajdzMini(neighborpunkt, cel);
			neighborpunkt.clear();
		}
		// jezeli punkt znajduje sie na prawej grawedzi
		else if (punkt.x == dlugosc - 1 && punkt.y > 0 && punkt.y < wysokosc) {
			if (ZamkSciezki[punkt.x - 1][punkt.y - 1] != GRASS &&
				ZamkSciezki[punkt.x][punkt.y - 1] != GRASS && ZamkSciezki[punkt.x][punkt.y + 1] != GRASS &&
				ZamkSciezki[punkt.x - 1][punkt.y + 1] != GRASS && ZamkSciezki[punkt.x - 1][punkt.y] != GRASS) {	// wokol punkt nie ma wolnego punktu
				if (path->size() > 0) {	//jezeli moze sie cofnac
					punkt = path->at(path->size() - 1);
					path->pop_back();
				} else {
					std::cout << " There is no path to destination " << endl;
					neighborpunkt.clear();
					for (int i = 0; i < wysokosc; i++)
						delete ZamkSciezki[i];
					delete[] ZamkSciezki;
					return path;
				}
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x, punkt.y + 1));
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y + 1));
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y));
			}
			punkt = ZnajdzMini(neighborpunkt, cel);
			neighborpunkt.clear();
		}
		// jezeli punkt znajduje sie na dolnej krawedzi
		else if (punkt.y == wysokosc - 1 && punkt.x > 0 && punkt.x < dlugosc) {
			if (ZamkSciezki[punkt.x - 1][punkt.y - 1] != GRASS &&
				ZamkSciezki[punkt.x][punkt.y - 1] != GRASS && ZamkSciezki[punkt.x + 1][punkt.y - 1] != GRASS && ZamkSciezki[punkt.x + 1][punkt.y] != GRASS &&
				ZamkSciezki[punkt.x - 1][punkt.y] != GRASS) {	// wokol punkt nie ma wolnego punktu
				if (path->size() > 0) {	//jezeli moze sie cofnac
					punkt = path->at(path->size() - 1);
					path->pop_back();
				} else {
					std::cout << " There is no path to destination " << endl;
					neighborpunkt.clear();
					for (int i = 0; i < wysokosc; i++)
						delete ZamkSciezki[i];
					delete[] ZamkSciezki;
					return path;
				}
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y));
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y));
			}
			cout << "(" << punkt.x << ", " << punkt.y << ") ";
			punkt = ZnajdzMini(neighborpunkt, cel);
			neighborpunkt.clear();
		}
		// jezeli punkt znajduje sie na lewej krawedzi
		else if (punkt.x == 0 && punkt.y > 0 && punkt.y < wysokosc) {
			if (ZamkSciezki[punkt.x][punkt.y - 1] != GRASS && ZamkSciezki[punkt.x + 1][punkt.y - 1] != GRASS && ZamkSciezki[punkt.x + 1][punkt.y] != GRASS &&
				ZamkSciezki[punkt.x + 1][punkt.y + 1] != GRASS && ZamkSciezki[punkt.x][punkt.y + 1] != GRASS) {	// wokol punkt nie ma wolnego punktu
				if (path->size() > 0) {	//jezeli moze sie cofnac
					punkt = path->at(path->size() - 1);
					path->pop_back();
				} else {
					std::cout << " There is no path to destination " << endl;
					neighborpunkt.clear();
					for (int i = 0; i < wysokosc; i++)
						delete ZamkSciezki[i];
					delete[] ZamkSciezki;
					return path;
				}
			}
			if (ZamkSciezki[punkt.x][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y));
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y + 1));
			}
			if (ZamkSciezki[punkt.x][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x, punkt.y + 1));
			}
			punkt = ZnajdzMini(neighborpunkt, cel);
			neighborpunkt.clear();
		}
	}
	ZamkSciezki[punkt.x][punkt.y] = TileType::BLOCKED;
	path->push_back(punkt);

	neighborpunkt.clear();
	for (int i = 0; i < wysokosc; i++)
		delete ZamkSciezki[i];
	delete[] ZamkSciezki;
	ZamkSciezki = NULL;

	return path;
}

std::vector<sf::Vector2i>* FindPath::SzukajSciezki(sf::Vector2i & poczatek, sf::Vector2i & cel, AGwiazdkaInit & init) {
	cout << "FIND PATH ------------------------------------------------------" << endl;
	cout << "Poczatek : " << poczatek.x << " " << poczatek.y << endl;
	cout << "cel : " << cel.x << " " << cel.y << endl;
	int dlugosc = mapa->GetDlugosc();
	int wysokosc = mapa->GetWysokosc();

	//inicjalizacja
	ZamkSciezki = new TileType*[wysokosc];
	sf::Vector2i punkt(poczatek);	// punkt wzgledem ktorego algorytm wyszukuje sciezki

	std::vector<sf::Vector2i> neighborpunkt;
	neighborpunkt.reserve(8);
	std::vector<sf::Vector2i> *path;	//kordynaty wynikow
	path = new vector<sf::Vector2i>;

	for (int i = 0; i < wysokosc; i++)	ZamkSciezki[i] = new TileType[dlugosc];	//	macierz zamknietych tras
	//funktor
	init(ZamkSciezki,wysokosc,dlugosc,cel);
	//PokazMape(poczatek, cel);
	/////////////////////////
	//glowna czesc algorytmu
	/////////////////////////
	while (!(punkt.x == cel.x && punkt.y == cel.y)) {
		ZamkSciezki[punkt.x][punkt.y] = BLOCKED;
		path->push_back(punkt);
		//jezeli punkt nie znajduje sie na krawedzi
		if (punkt.x > 0 && punkt.x < dlugosc && punkt.y > 0 && punkt.y < wysokosc) {
			if (ZamkSciezki[punkt.x - 1][punkt.y - 1] != GRASS &&
				ZamkSciezki[punkt.x][punkt.y - 1] != GRASS && ZamkSciezki[punkt.x + 1][punkt.y - 1] != GRASS && ZamkSciezki[punkt.x + 1][punkt.y] != GRASS &&
				ZamkSciezki[punkt.x + 1][punkt.y + 1] != GRASS && ZamkSciezki[punkt.x][punkt.y + 1] != GRASS && ZamkSciezki[punkt.x - 1][punkt.y + 1] != GRASS &&
				ZamkSciezki[punkt.x - 1][punkt.y] != GRASS) {	// wokol punkt nie ma wolnego punktu
				if (path->size() > 0) {	//jezeli moze sie cofnac
					punkt = path->at(path->size() - 1);
					path->pop_back();
				} else {
					std::cout << " There is no path to destination " << endl;
					neighborpunkt.clear();
					for (int i = 0; i < wysokosc; i++)
						delete ZamkSciezki[i];
					delete[] ZamkSciezki;
					return path;
				}
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y));
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y + 1));
			}
			if (ZamkSciezki[punkt.x][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x, punkt.y + 1));
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y + 1));
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y));
			}
			punkt = ZnajdzMini(neighborpunkt, cel);
			neighborpunkt.clear();
		}
		// jezeli punkt znajduje sie przy gornej krawedzi
		else if (punkt.y == 0 && punkt.x > 0 && punkt.x < dlugosc) {
			if (ZamkSciezki[punkt.x + 1][punkt.y] != GRASS &&
				ZamkSciezki[punkt.x + 1][punkt.y + 1] != GRASS && ZamkSciezki[punkt.x][punkt.y + 1] != GRASS && ZamkSciezki[punkt.x - 1][punkt.y + 1] != GRASS &&
				ZamkSciezki[punkt.x - 1][punkt.y] != GRASS) {	// wokol punkt nie ma wolnego punktu
				if (path->size() > 0) {	//jezeli moze sie cofnac
					punkt = path->at(path->size() - 1);
					path->pop_back();
				} else {
					std::cout << " There is no path to destination " << endl;
					neighborpunkt.clear();
					for (int i = 0; i < wysokosc; i++)
						delete ZamkSciezki[i];
					delete[] ZamkSciezki;
					return path;
				}
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y));
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y + 1));
			}
			if (ZamkSciezki[punkt.x][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x, punkt.y + 1));
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y + 1));
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y));
			}
			punkt = ZnajdzMini(neighborpunkt, cel);
			neighborpunkt.clear();
		}
		// jezeli punkt znajduje sie na prawej grawedzi
		else if (punkt.x == dlugosc - 1 && punkt.y > 0 && punkt.y < wysokosc) {
			if (ZamkSciezki[punkt.x - 1][punkt.y - 1] != GRASS &&
				ZamkSciezki[punkt.x][punkt.y - 1] != GRASS && ZamkSciezki[punkt.x][punkt.y + 1] != GRASS &&
				ZamkSciezki[punkt.x - 1][punkt.y + 1] != GRASS && ZamkSciezki[punkt.x - 1][punkt.y] != GRASS) {	// wokol punkt nie ma wolnego punktu
				if (path->size() > 0) {	//jezeli moze sie cofnac
					punkt = path->at(path->size() - 1);
					path->pop_back();
				} else {
					std::cout << " There is no path to destination " << endl;
					neighborpunkt.clear();
					for (int i = 0; i < wysokosc; i++)
						delete ZamkSciezki[i];
					delete[] ZamkSciezki;
					return path;
				}
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x, punkt.y + 1));
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y + 1));
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y));
			}
			punkt = ZnajdzMini(neighborpunkt, cel);
			neighborpunkt.clear();
		}
		// jezeli punkt znajduje sie na dolnej krawedzi
		else if (punkt.y == wysokosc - 1 && punkt.x > 0 && punkt.x < dlugosc) {
			if (ZamkSciezki[punkt.x - 1][punkt.y - 1] != GRASS &&
				ZamkSciezki[punkt.x][punkt.y - 1] != GRASS && ZamkSciezki[punkt.x + 1][punkt.y - 1] != GRASS && ZamkSciezki[punkt.x + 1][punkt.y] != GRASS &&
				ZamkSciezki[punkt.x - 1][punkt.y] != GRASS) {	// wokol punkt nie ma wolnego punktu
				if (path->size() > 0) {	//jezeli moze sie cofnac
					punkt = path->at(path->size() - 1);
					path->pop_back();
				} else {
					std::cout << " There is no path to destination " << endl;
					neighborpunkt.clear();
					for (int i = 0; i < wysokosc; i++)
						delete ZamkSciezki[i];
					delete[] ZamkSciezki;
					return path;
				}
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y));
			}
			if (ZamkSciezki[punkt.x - 1][punkt.y] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x - 1, punkt.y));
			}
			cout << "(" << punkt.x << ", " << punkt.y << ") ";
			punkt = ZnajdzMini(neighborpunkt, cel);
			neighborpunkt.clear();
		}
		// jezeli punkt znajduje sie na lewej krawedzi
		else if (punkt.x == 0 && punkt.y > 0 && punkt.y < wysokosc) {
			if (ZamkSciezki[punkt.x][punkt.y - 1] != GRASS && ZamkSciezki[punkt.x + 1][punkt.y - 1] != GRASS && ZamkSciezki[punkt.x + 1][punkt.y] != GRASS &&
				ZamkSciezki[punkt.x + 1][punkt.y + 1] != GRASS && ZamkSciezki[punkt.x][punkt.y + 1] != GRASS) {	// wokol punkt nie ma wolnego punktu
				if (path->size() > 0) {	//jezeli moze sie cofnac
					punkt = path->at(path->size() - 1);
					path->pop_back();
				} else {
					std::cout << " There is no path to destination " << endl;
					neighborpunkt.clear();
					for (int i = 0; i < wysokosc; i++)
						delete ZamkSciezki[i];
					delete[] ZamkSciezki;
					return path;
				}
			}
			if (ZamkSciezki[punkt.x][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y - 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y - 1));
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y));
			}
			if (ZamkSciezki[punkt.x + 1][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x + 1, punkt.y + 1));
			}
			if (ZamkSciezki[punkt.x][punkt.y + 1] == GRASS) {
				neighborpunkt.push_back(sf::Vector2i(punkt.x, punkt.y + 1));
			}
			punkt = ZnajdzMini(neighborpunkt, cel);
			neighborpunkt.clear();
		}
	}
	ZamkSciezki[punkt.x][punkt.y] = TileType::BLOCKED;
	path->push_back(punkt);

	neighborpunkt.clear();
	for (int i = 0; i < wysokosc; i++)
		delete ZamkSciezki[i];
	delete[] ZamkSciezki;
	ZamkSciezki = NULL;

	return path;
}

ostream& FindPath::WypiszSciezke(ostream & str, sf::Vector2i & poczatek, sf::Vector2i & cel) {
	return str;
}

void FindPath::PokazMape(sf::Vector2i & from, sf::Vector2i & destination) {
	for (int i = 0; i < mapa->GetWysokosc(); i++) {	//inicjalizacja macierzy zamknietych tras
		for (int j = 0; j < mapa->GetDlugosc(); j++) {
			if (ZamkSciezki[j][i] == TileType::BLOCKED)
				cout << "2";
			else if (ZamkSciezki[j][i] == GRASS)
				cout << "1";
		}
		cout << endl;
	}
}

void FindPath::SetMap(Map & map) {
	this->mapa = &map;
}

void FindPath::init() {
}

sf::Vector2i FindPath::ZnajdzMini(std::vector<sf::Vector2i> tab, sf::Vector2i & cel) {
	sf::Vector2i mini = tab[0];
	double distanceMini = 0;	//	najmniejsza odleglosc z tablicy tab
	double * distanceTab = new double[tab.size()];	//przechowuje dlugosci kaddego punktu od destination aby moc je pozniej porownac

	//wyliczanie odleglosci kazdego punktu od celu
	for (int i = 0; i < tab.size(); i++) {
		double distance = sqrt(((cel.x - tab[i].x) * (cel.x - tab[i].x)) + ((cel.y - tab[i].y)*(cel.y - tab[i].y)));
		distanceTab[i] = distance;
	}
	//znajdowanie najmniejszej wartosci
	distanceMini = distanceTab[0];
	for (int i = 0; i < tab.size(); i++) {
		if (distanceMini > distanceTab[i]) {
			distanceMini = distanceTab[i];
			mini = tab[i];
		}
	}

	return mini;
}