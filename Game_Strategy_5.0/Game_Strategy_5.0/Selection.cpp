#include "Selection.h"

Selection* Selection::Obiekt = NULL;

Selection::Selection() {
	ObszarZaznaczenia.setFillColor(sf::Color(147, 246, 0, 0));
	//KOLORLINIZAZNACZONEJ
	sf::Color kolor(0,255,0,50);
	ObszarZaznaczenia.setOutlineColor(kolor);
	//SZEROKOSCLINIZAZNACZONEJ
	float wielkosc = 1;
	ObszarZaznaczenia.setOutlineThickness(wielkosc);

	m_b_isSelection = false;
}

void Selection::Stworz() {
	if (!Obiekt)
		Obiekt = new Selection;
	else {
		delete Obiekt;
		Obiekt = new Selection;
	}
}

void Selection::Destroy() {
	delete Obiekt;
	Obiekt = NULL;
}

Selection& Selection::GetObiekt() {
		return *Obiekt;
}

void Selection::start(sf::Vector2f & mouse) {
	ObszarZaznaczenia.setPosition(mouse);
	m_b_isSelection = true;
	ObszarZaznaczenia.setSize(sf::Vector2f(0, 0));
}

void Selection::stop(sf::Vector2f & mouse) {
	sf::Vector2f f_end = mouse;
	sf::Vector2f f_start = ObszarZaznaczenia.getPosition();

	if (f_start.x > f_end.x)
		std::swap(f_end.x, f_start.x);
	if (f_start.y > f_end.y)
		std::swap(f_end.y, f_start.y);

	ObszarZaznaczenia.setPosition(f_start);
	ObszarZaznaczenia.setSize(f_end - f_start);
	m_b_isSelection = false;
}

sf::RectangleShape& Selection::getSelectionArea() {
	return ObszarZaznaczenia;
}

bool & Selection::isSelectionMode() {
	return m_b_isSelection;
}

void Selection::update(sf::Vector2f & mouse) {
	sf::Vector2f size(mouse);

	size.x -= ObszarZaznaczenia.getPosition().x;
	size.y -= ObszarZaznaczenia.getPosition().y;
	//cout << size.x << " " << size.y << endl;
	ObszarZaznaczenia.setSize(size);
}

void Selection::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	target.draw(ObszarZaznaczenia);
}
