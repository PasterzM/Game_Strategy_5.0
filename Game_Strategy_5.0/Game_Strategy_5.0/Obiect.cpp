#include <iostream>
#include "Obiect.h"
#include "Map.h"
#include "TextureManager.h"
#include "MessageManager.h"
#include "Editor.h"



luabridge::lua_State* Obiect::L = NULL;
int Obiect::v_count = 1;

using namespace std;
using namespace ObiectNameSpace;

namespace ObiectNameSpace {
	string ObiectDirectionToString(ObiectDirection arg) {
		switch (arg) {
		case STOP: return "STOP"; break;
		case N: return "N"; break;
		case NE: return "NE"; break;
		case E: return "E"; break;
		case SE: return "SE"; break;
		case S: return "S"; break;
		case WS: return "WS"; break;
		case W: return "W"; break;
		case NW: return "NW"; break;
		default: return "WRONG"; break;
		}
		return "WRONG";
	}
	ObiectDirection StringToObiectDirection(string arg) {
		if (arg == "STOP")	return STOP;
		if (arg == "N")	return N;
		if (arg == "NE")	return NE;
		if (arg == "E")	return E;
		if (arg == "SE")	return SE;
		if (arg == "S")	return S;
		if (arg == "WS")	return WS;
		if (arg == "W")	return W;
		if (arg == "NW")	return NW;
		return STOP;
	}
};

Obiect::Obiect(const Obiect* prototype, float posX, float posY, Player& player) :
	Obiect(*prototype) {
	this->SetPosition(posX, posY);
	v_isSelected = false;
	v_idPlayer = &player;
	v_id = v_count;
	++v_count;
	v_ObiectType = ObiectType::OBIECT;
	v_numOfButtons = 9;	//TODO: przyciski odczytywane z pliku
	if (!v_isBuilding) {
		v_direction = ObiectDirection::STOP;
		v_sprite.setTextureRect((*(*p_animationFrames)[v_direction])[0]);
	}
}

Obiect::Obiect(const string& name, const  string& file) :
	v_id(0),
	v_direction(ObiectDirection::S),
	p_animationFrames(NULL),
	v_ObiectType(ObiectType::PROTOTYPE) {
	if (!LoadScript(file.c_str(), name))
		throw exception("Problem przy tworzeniu wzoru obiektu");
	(*LuaFunc.at("InitScripts"))(this);

	//TODO:zmiana na edytorze ikony jednostek
	v_spriteIcon.setTexture(*ManadzerTexture::GetManadzer()->GetMap().at("Icons"));

	//Animacje
	p_animationFrames = new Table<Table<sf::IntRect>*>(9);	// macierz 3x3
	(*p_animationFrames)[ObiectNameSpace::ObiectDirection::STOP] = new Table<sf::IntRect>(5);
	for (int i = 1; i < p_animationFrames->m_length; ++i) {
		(*p_animationFrames)[i] = new Table<sf::IntRect>(4, sf::IntRect(0, 0, 0, 0));
	}

	p_attackFrames = new Table<Table<sf::IntRect>*>(9);	//animacje ataku
	for (int i = 0; i < p_attackFrames->m_length; ++i) {
		(*p_attackFrames)[i] = new Table<sf::IntRect>(4, sf::IntRect(0, 0, 0, 0));
	}

	p_deathFrames = new Table<Table<sf::IntRect>*>(9);	//animacje ataku
	for (int i = 0; i < p_deathFrames->m_length; ++i) {
		(*p_deathFrames)[i] = new Table<sf::IntRect>(4, sf::IntRect(0, 0, 0, 0));
	}
}

Obiect::~Obiect() {
	if (v_ObiectType == ObiectNameSpace::ObiectType::PROTOTYPE) {
		for (int i = 0; i < p_animationFrames->m_length; ++i)
			delete (*p_animationFrames)[i];
		delete p_animationFrames;

		for (int i = 0; i < p_attackFrames->m_length; ++i)
			delete (*p_attackFrames)[i];
		delete p_attackFrames;

		for (int i = 0; i < p_deathFrames->m_length; ++i)
			delete (*p_deathFrames)[i];
		delete p_deathFrames;
	}
}

void Obiect::RegisterClassObiect(luabridge::lua_State * L) {
	Obiect::L = L;
	//wysylanie danych o klasie do lua
	getGlobalNamespace(L)
		.beginClass<Obiect>("Obiect")
		.addConstructor<void(*)(void)>()
		.addProperty("v_name", &Obiect::GetName, &Obiect::SetName)
		.addProperty("v_isBuilding", &Obiect::GetIsBuilding, &Obiect::SetIsBuilding)
		.addProperty("v_sprite", &Obiect::Getsprite, &Obiect::Setsprite)
		.addProperty("v_texture", &Obiect::Gettexture, &Obiect::SetTexture)
		.addProperty("v_circle", &Obiect::Getcircle, &Obiect::Setcircle)
		.addProperty("v_speed", &Obiect::Getspeed, &Obiect::Setspeed)
		.addProperty("v_speedMoveAnimation", &Obiect::GetspeedMoveAnimation, &Obiect::SetspeedMoveAnimation)
		.addProperty("v_speedDeathAnimation", &Obiect::GetspeedDeathAnimation, &Obiect::SetspeedDeathAnimation)
		.addProperty("v_speedAttackAnimation", &Obiect::GetspeedAttackAnimation, &Obiect::SetspeedAttackAnimation)
		.addProperty("v_defence", &Obiect::Getdefence, &Obiect::Setdefence)
		.addProperty("v_strength", &Obiect::Getstrength, &Obiect::Setstrength)
		.addProperty("v_life", &Obiect::Getlife, &Obiect::Setlife)
		.addProperty("v_maxLife", &Obiect::GetmaxLife, &Obiect::SetmaxLife)
		.addProperty("v_mana", &Obiect::Getmana, &Obiect::Setmana)
		.addProperty("v_maxMana", &Obiect::GetmaxMana, &Obiect::SetmaxMana)
		.addProperty("v_timeBuilding", &Obiect::GettimeBuilding, &Obiect::SettimeBuilding)
		.addProperty("v_foodCost", &Obiect::GetfoodCost, &Obiect::SetfoodCost)
		.addProperty("v_circleColor", &Obiect::GetColor, &Obiect::SetColor)
		.addProperty("v_size", &Obiect::GetSize, &Obiect::SetSize)
		.addProperty("v_player", &Obiect::GetPlayer)
		.addProperty("v_observer", &Obiect::GetObserver)
		.addProperty("v_mode", &Obiect::GetModeToString, &Obiect::SetMode)
		.addProperty("v_TransportingMineType", &Obiect::GetTransportingType, &Obiect::SetTransporingType)
		.addFunction("SetPosition", &Obiect::SetPosition)
		.addFunction("Move", &Obiect::Move)
		.addFunction("LoadScriptFunction", &Obiect::LoadScriptFunction)
		.addFunction("AddToListOfButtons", &Obiect::AddToListOfButtons)
		//.addFunction("MessageManager",&MessageManager::GetObject)
		//.addFunction("AddMessage",&MessageManager::AddMessage)
		.endClass();
}

void Obiect::draw(sf::RenderWindow & window) {
#ifdef _DEBUG || DEBUG
	window.draw(v_circle);
#endif // _DEBUG
	window.draw(v_sprite);
}

sf::Sprite& Obiect::GetSriteIcon() {
	return v_spriteIcon;
}

sf::Sprite & Obiect::GetSprite() {
	return v_sprite;
}

string Obiect::GetName() const {
	return v_name;
}

sf::Sprite  Obiect::Getsprite() const {
	return v_sprite;
}

sf::Texture  Obiect::Gettexture() const {
	return v_texture;
}

sf::CircleShape  Obiect::Getcircle() const {
	return v_circle;
}

float  Obiect::Getspeed() const {
	return v_speed;
}

float  Obiect::GetspeedMoveAnimation() const {
	return v_speedMoveAnimation;
}

float  Obiect::GetspeedDeathAnimation() const {
	return v_speedDeathAnimation;
}

float  Obiect::GetspeedAttackAnimation() const {
	return v_speedAttackAnimation;
}

int  Obiect::Getdefence() const {
	return v_defence;
}

int  Obiect::Getstrength() const {
	return v_strength;
}

int  Obiect::Getlife() const {
	return v_life;
}

int  Obiect::GetmaxLife() const {
	return v_maxLife;
}

int  Obiect::Getmana() const {
	return v_mana;
}

int  Obiect::GetmaxMana() const {
	return v_maxMana;
}

float  Obiect::GettimeBuilding() const {
	return v_timeBuilding;
}

int  Obiect::GetfoodCost() const {
	return v_foodCost;
}

void Obiect::SetName(string arg) {
	v_name = arg;
}

void Obiect::Setsprite(sf::Sprite arg) {
	v_sprite = arg;
}

void Obiect::SetSprite(sf::Texture& arg) {
	v_sprite.setTexture(arg);
}

void Obiect::setRadius(float rect) {
	v_circle.setRadius(rect);
}

void Obiect::SetTexture(sf::Texture arg) {
	v_texture = arg;
}

void Obiect::Setcircle(sf::CircleShape arg) {
	v_circle = arg;
}

void Obiect::Setspeed(float arg) {
	v_speed = arg;
}

void Obiect::SetspeedMoveAnimation(float arg) {
	v_speedMoveAnimation = arg;
}

void Obiect::SetspeedDeathAnimation(float arg) {
	v_speedDeathAnimation = arg;
}

void Obiect::SetspeedAttackAnimation(float arg) {
	v_speedAttackAnimation = arg;
}

void Obiect::Setdefence(int arg) {
	v_defence = arg;
}

void Obiect::Setstrength(int arg) {
	v_strength = arg;
}

void Obiect::Setlife(int arg) {
	v_life = arg;
}

void Obiect::SetmaxLife(int arg) {
	v_maxLife = arg;
}

void Obiect::Setmana(int arg) {
	v_mana = arg;
}

void Obiect::SetmaxMana(int arg) {
	v_maxMana = arg;
}

void Obiect::SettimeBuilding(float arg) {
	v_timeBuilding = arg;
}

void Obiect::SetfoodCost(int arg) {
	v_foodCost = arg;
}

void Obiect::SetSize(int arg) {
	v_size = arg;
	v_circle.setRadius((float)(v_size / 2));
	sf::IntRect newSize(v_sprite.getTextureRect().left, v_sprite.getTextureRect().top, v_size, v_size);
	v_sprite.setTextureRect(newSize);

}

void Obiect::SetColor(int *arg) {
	sf::Color col(arg[0], arg[1], arg[2], arg[3]);
	v_circle.setFillColor(col);
	delete[] arg;
}

sf::Texture & Obiect::GetTexture() {
	return v_texture;
}

void Obiect::printData() {
	cout << "v_name " << v_name << " " << "v_speed " << v_speed << " " << "v_speedMoveAnimation " << v_speedMoveAnimation << endl
		<< "v_speedDeathAnimation " << v_speedDeathAnimation << " " << "v_speedAttackAnimation " << v_speedAttackAnimation << " " << "v_defence " << v_defence << endl
		<< "v_strength " << v_strength << " " << "v_life " << v_life << " " << "v_maxLife " << v_maxLife << endl
		<< "v_mana " << v_mana << " " << "v_maxMana " << v_maxMana << " " << "v_timeBuilding " << v_timeBuilding << endl
		<< "v_foodCost " << v_foodCost << "v_isBuilding " << v_isBuilding << endl;
}

bool Obiect::LoadScript(string scriptName) {
	using namespace luabridge;
	LuaRef table = getGlobal(L, v_name.c_str());
	std::shared_ptr<luabridge::LuaRef> iterToFunction;
	if (table.isTable()) {
		if (table[scriptName.c_str()].isFunction()) {
			iterToFunction = std::make_shared<LuaRef>(table[scriptName]);
			LuaFunc[scriptName] = iterToFunction;
			return true;
		} else {
			cout << "error with move" << endl;
			iterToFunction.reset();
			return false;
		}
	}
}

bool Obiect::executeScript(string scriptName) {
	try {
		if (LuaFunc.at(scriptName)) {
			(*LuaFunc.at(scriptName))(this);
			return true;
		}
	} catch (luabridge::LuaException const& e) {
		std::cout << "LuaException: " << e.what() << std::endl;
		return false;
	} catch (exception e) {
		std::cout << "niema takiego skryptu w pliku " << *v_fileName << std::endl;
		return false;
	}
	return false;
}

void Obiect::SetIsBuilding(bool is) {
	v_isBuilding = is;
}

void Obiect::SetPosition(float x, float y) {
	v_sprite.setPosition(x, y);
	v_circle.setPosition(x, y);
}

void Obiect::Move(float x, float y) {
	v_circle.move(x, y);
	v_sprite.setPosition(x, y);
}

bool Obiect::LoadScriptfunctionFromWindow(string tab) {
	luabridge::LuaRef table = getGlobal(L, v_name.c_str());
	if (table.isTable() && table[tab].isFunction()) {
		if (LuaFunc.find(tab) == LuaFunc.end()) {
			LuaFunc[tab] = std::make_shared<luabridge::LuaRef>(table[tab]);
			cout << "Udalo sie wczytac skrypt " << tab << endl;
			return true;
		} else {	//jezeli skrypt jest juz wczytany usun a nastepnie ponownie wstaw
					//LuaFunc[tab]->
			LuaFunc.erase(tab);
			LoadScriptfunctionFromWindow(tab);
		}
	} else
		return false;
}

void Obiect::setSelected(bool arg) {
	using namespace luabridge;
	v_isSelected = arg;

	if (v_isSelected) {
		v_circle.setFillColor(sf::Color(0, 0, 0));
	} else {
		v_circle.setFillColor(sf::Color(128, 128, 128));
	}

	//LuaRef tableColor = getGlobal(L, "v_circleSelectedColor");	//TODO:nie dzialana razie
	//if (arg) {
	//	if (tableColor.isTable()) {
	//		sf::Color col(tableColor["r"].cast<int>(), tableColor["g"].cast<int>(), tableColor["b"].cast<int>(), tableColor["alpha"].cast<int>());
	//		v_circle.setFillColor(col);

	//	}else {//na razie bo nie dziala*/
	//		sf::Color col(255, 255, 255, 255);
	//		v_circle.setFillColor(col);
	//	}
	//} else {	//odznaczanie
	//	LuaRef tableColor = getGlobal(L, "v_circleColor");
	//	if (tableColor.isTable()) {
	//		sf::Color col(tableColor["r"].cast<int>(), tableColor["g"].cast<int>(), tableColor["b"].cast<int>(), tableColor["alpha"].cast<int>());
	//		v_circle.setFillColor(col);
	//	} else {
	//		sf::Color col(0, 0, 0, 255);
	//		v_circle.setFillColor(col);
	//	}
	//}
}

const sf::IntRect& Obiect::GetTextureRect() {
	return v_sprite.getTextureRect();
}


void Obiect::SetPath(vector<sf::Vector2i>& path) {
	if (v_vec_path) {
		delete v_vec_path;
	}
	v_mode = ObiectMode::MOVING;
	v_vec_path = new vector<sf::Vector2i>(path);
	nastPoz = (*v_vec_path->begin());
	v_vec_path->erase(v_vec_path->begin());
	ObserwowanyObiekt = Map::GetObiekt().getPlytka(path.back().x, path.back().y).Wsk;
}

void Obiect::Update(sf::Time & dt) {
	switch (v_mode) {
	case ObiectMode::NOTHING:
		//AktualizujObiektObserwowany();
		break;
	case ObiectMode::WAITING:
		//AktualizujObiektObserwowany();
		break;
	case ObiectMode::MOVING:
		move(dt);
		break;
	case ObiectMode::CREATING:
		//Ruch(dt);
		break;
	case ObiectMode::BUILD:
		//
		break;
	case ObiectMode::ATTACKING:
		attack(dt);
		break;
	case ObiectMode::DIGGING:
		dig(dt);
		break;
	case ObiectMode::APPLY:
		apply(dt);
		break;
	}
}

void Obiect::AktualizujObiektObserwowany() {
#ifdef _DEBUG || DEBUG
	try {
		LuaFunc.at("Interaction")->operator()(this, MessageManager::GetObject());
	} catch (luabridge::LuaException e) {
		cout << e.what() << endl;
	}
#else
	try {
		LuaFunc.at("Interaction")->operator()(this, MessageManager::GetObject());
	} catch (luabridge::LuaException e) {
		cout << e.what() << endl;
	}
#endif // _DEBUG || DEBUG
}

void Obiect::DrawInterface(sf::RenderWindow & ref) {
	ref.draw(v_spriteIcon);
	//TODO:rysowanie przyciskow
}

void Obiect::AddToListOfButtons(string name) {
	v_VecOfButtons.push_back(name);
}

vector<string>& Obiect::GetListOfButtons() {
	return v_VecOfButtons;
}

Table<Table<sf::IntRect>*>& Obiect::GetFrameAnimationTable() {
	return *p_animationFrames;
}

Table<Table<sf::IntRect>*>& Obiect::GetFrameAttackTable() {
	return 	*p_attackFrames;
}

Table<Table<sf::IntRect>*>& Obiect::GetFrameDeathTable() {
	return *p_deathFrames;
}

ObiectNameSpace::ObiectDirection & Obiect::GetDirection() {
	return v_direction;
}

float & Obiect::GetMoveAnimationSpeed() {
	return v_speedMoveAnimation;
}

void Obiect::SetMoveAnimationSpeed(float speed) {
	v_speedMoveAnimation = speed;
}

void Obiect::SetSpritePath(const string & arg) {
	v_texturePath = arg;
}

string & Obiect::GetSpritePath() {
	return v_texturePath;
}

bool Obiect::operator==(const Obiect & ref) {
	if (v_name == ref.GetName() && v_sprite.getPosition() == ref.Getsprite().getPosition())
		return true;
	return false;
}

string Obiect::GetModeToString() const {
	switch (v_mode) {
	case ObiectMode::NOTHING: return string("NOTHING");
	case ObiectMode::WAITING: return string("WAITING");
	case ObiectMode::BUILD: return string("BUILD");
	case ObiectMode::CREATING: return string("CREATING");
	case ObiectMode::MOVING: return string("MOVING");
	case ObiectMode::ATTACKING: return string("ATTACKING");
	case ObiectMode::DIGGING: return string("DIGGING");
	}
	return string("NOTHING");
}

ObiectNameSpace::ObiectMode Obiect::GetStringToMode(string mode) {
	if (mode == "NOTHING")	return ObiectMode::NOTHING;
	else if (mode == "WAITING")	return ObiectMode::WAITING;
	else if (mode == "BUILD")	return ObiectMode::BUILD;
	else if (mode == "CREATING")	return ObiectMode::CREATING;
	else if (mode == "MOVING")	return ObiectMode::MOVING;
	else if (mode == "ATTACKING")	return ObiectMode::ATTACKING;
	else if (mode == "DIGGING") return ObiectMode::DIGGING;
	return ObiectMode::NOTHING;
}

void Obiect::SetMode(string mode) {
	v_actualframe = 0;
	v_actualNumberOfHits = 0;
	if (mode == "NOTHING") {
		v_mode = ObiectMode::NOTHING;	return;
	} else if (mode == "WAITING") {
		v_mode = ObiectMode::WAITING;	return;
	} else if (mode == "BUILD") {
		v_mode = ObiectMode::BUILD;	return;
	} else if (mode == "CREATING") {
		v_mode = ObiectMode::CREATING;	return;
	} else if (mode == "MOVING") {
		v_mode = ObiectMode::MOVING;	return;
	} else if (mode == "ATTACKING") {
		v_mode = ObiectMode::ATTACKING;	return;
	} else if (mode == "DIGGING") {
		v_mode = ObiectMode::DIGGING; return;
	} else {
		v_mode = ObiectMode::NOTHING;
	}
}

void Obiect::SetModeByType(ObiectNameSpace::ObiectMode mode){
	v_actualframe = 0;
	v_actualNumberOfHits = 0;
	v_mode = mode;
}

void Obiect::SetTransporingType(string arg) {
	if (arg == "EMPTY") {
		v_typeOfTransportingMineral = ObiectMineType::EMPTY;
	} else if (arg == "GOLD") {
		v_typeOfTransportingMineral = ObiectMineType::GOLD;
	} else if (arg == "METAL") {
		v_typeOfTransportingMineral = ObiectMineType::METAL;
	} else {
		v_typeOfTransportingMineral = ObiectMineType::EMPTY;
	}
}

bool Obiect::GetIsBuilding()const {
	return v_isBuilding;
}

int Obiect::GetSize() const {
	return v_size;
}

int Obiect::GetColor() const {
	return 0;
}

const sf::Vector2f& Obiect::GetPosition() const {
	return v_circle.getPosition();
}

const int & Obiect::GetId() {
	return v_id;
}

Player& Obiect::GetPlayer() const {
	return *v_idPlayer;
}

Obiect& Obiect::GetObserver() const {
	return *dynamic_cast<Obiect*>(ObserwowanyObiekt);
}

string & Obiect::GetTransportingType()const {
	switch (v_typeOfTransportingMineral) {
	case ObiectMineType::EMPTY:	return string("EMPTY");
	case ObiectMineType::GOLD:	return string("GOLD");
	case ObiectMineType::METAL:	return string("METAL");
	}
}

void Obiect::move(sf::Time & dt) {

	v_actualAnimationTime += dt.asSeconds();
	if (v_speedMoveAnimation <= v_actualAnimationTime) {	//zmiana textury
		++v_actualframe;
		v_actualAnimationTime = 0;
		if (v_actualframe >= 4)
			v_actualframe = 0;
		v_sprite.setTextureRect((*(*p_animationFrames)[v_direction])[v_actualframe]);
	}

	sf::Vector2f PozycjaObiektu(v_circle.getPosition());
	sf::Vector2f nastPozObiektu(nastPoz);

	nastPozObiektu.x *= Map::GetWielkoscPlytki().x;
	nastPozObiektu.y *= Map::GetWielkoscPlytki().y;

	if (PozycjaObiektu != nastPozObiektu && nastPoz.x != 0 && nastPoz.y != 0) {
		//poruszanie jednostka
		move(sf::Vector2f(WektorKierunkowy.x*dt.asSeconds()*v_speed, WektorKierunkowy.y*dt.asSeconds()*v_speed));

		if (WektorKierunkowy.x > 0 && PozycjaObiektu.x > nastPozObiektu.x) {
			setPosition(nastPozObiektu);
			PozycjaObiektu = nastPozObiektu;
		} else if (WektorKierunkowy.x < 0 && PozycjaObiektu.x < nastPozObiektu.x) {
			setPosition(nastPozObiektu);
			PozycjaObiektu = nastPozObiektu;
		}

		if (WektorKierunkowy.y > 0 && PozycjaObiektu.y > nastPozObiektu.y) {
			setPosition(nastPozObiektu);
			PozycjaObiektu = nastPozObiektu;
		} else if (WektorKierunkowy.y < 0 && PozycjaObiektu.y < nastPozObiektu.y) {
			setPosition(nastPozObiektu);
			PozycjaObiektu = nastPozObiektu;
		}
	} else {//nastepna pozycja
		//jezeli niema wiecej ruchow
		if (v_vec_path && v_vec_path->empty()) {
			//Tryb = Tryby::Jednostka::STOJ;
			v_direction = ObiectDirection::STOP;
			v_sprite.setTextureRect((*(*p_animationFrames)[v_direction])[v_direction]);
			WektorKierunkowy.x = 0;
			WektorKierunkowy.y = 0;
			delete v_vec_path;
			v_vec_path = NULL;
			return;
		} else if (v_vec_path) {
			//pobiera nastepny krok
			nastPoz = *v_vec_path->begin();
			v_vec_path->erase(v_vec_path->begin());

			nastPozObiektu = (sf::Vector2f)nastPoz;
			sf::Vector2f pozNaMapie(PozycjaObiektu);
			pozNaMapie.x /= Map::GetWielkoscPlytki().x;
			pozNaMapie.y /= Map::GetWielkoscPlytki().y;
			v_direction = CalculateDirection(pozNaMapie, nastPozObiektu);

			//ustawienie nastepnego punktu jezeli punkt jest wolny
			if (!Map::GetObiekt().getPlytka(nastPoz.x, nastPoz.y).Wsk)
				Map::GetObiekt().getPlytka(nastPoz.x, nastPoz.y).Wsk = this;
			else {
				//ulozenie obiektu na danej pozycji
				PozycjaObiektu.x /= Map::GetWielkoscPlytki().x;
				PozycjaObiektu.y /= Map::GetWielkoscPlytki().y;
				PozycjaObiektu.x *= Map::GetWielkoscPlytki().x;
				PozycjaObiektu.y *= Map::GetWielkoscPlytki().y;
				setPosition(PozycjaObiektu);
				v_mode = ObiectMode::NOTHING;
				//TODO: je�eli obiekt wszedl w kolizje z obiektem ktorym mial wejsc
				//cout << Mapa::GetObiekt().getPlytka(nastPoz).Wsk->GetNazwa() << " " << Map::GetObiekt().getPlytka(nastPoz).Wsk->GetId() << endl;
				//cout << dynamic_cast<Obiect*>(getObserwowany())->GetNazwa() << " " << dynamic_cast<Obiect*>(getObserwowany())->GetId() << endl;
				//cout << Map::GetObiekt().getPlytka(nastPoz).Wsk << " " << ObserwowanyObiekt << endl;
				if (Map::GetObiekt().getPlytka(nastPoz).Wsk == ObserwowanyObiekt) {
					AktualizujObiektObserwowany();
				}
				return;
			}
			//aktualna pozycja obiektu na macierzy jest ustawiana na pusta
			sf::Vector2f pozObiektuNaMacierzy;
			pozObiektuNaMacierzy.x = PozycjaObiektu.x / Map::GetWielkoscPlytki().x;
			pozObiektuNaMacierzy.y = PozycjaObiektu.y / Map::GetWielkoscPlytki().y;
			Map::GetObiekt().getPlytka((int)pozObiektuNaMacierzy.x, (int)pozObiektuNaMacierzy.y).Wsk = NULL;

			nastPozObiektu.x *= Map::GetWielkoscPlytki().x;
			nastPozObiektu.y *= Map::GetWielkoscPlytki().y;

			//Obliczenie kiedunku ruchu jednostki
			WektorKierunkowy.x = nastPozObiektu.x - PozycjaObiektu.x;
			WektorKierunkowy.y = nastPozObiektu.y - PozycjaObiektu.y;

			WektorKierunkowy = ObliczWektorKierunkowyJednostki(PozycjaObiektu, nastPozObiektu);
		} else {
			v_mode = ObiectMode::NOTHING;
			delete v_vec_path;
			v_vec_path = NULL;
		}
	}
}

void Obiect::attack(sf::Time & dt) {
	//TODO:animacja uderzenia
}

bool Obiect::LoadScriptFunction(string name) {
	luabridge::LuaRef table = getGlobal(L, v_name.c_str());
	if (table.isTable() && table[name.c_str()].isFunction()) {
		if (LuaFunc.find(name.c_str()) == LuaFunc.end()) {
			LuaFunc[name.c_str()] = std::make_shared<luabridge::LuaRef>(table[name.c_str()]);
			cout << "Udalo sie wczytac skrypt " << name << endl;
			return true;
		} else {	//jezeli skrypt jest juz wczytany usun a nastepnie ponownie wstaw
					//LuaFunc[tab]->
			LuaFunc.erase(name);
			LoadScriptfunctionFromWindow(name);
		}
	}
	return false;
}

ObiectNameSpace::ObiectDirection Obiect::CalculateDirection(sf::Vector2f & start, sf::Vector2f & end) {
	string sDirect;

	if (start.y > end.y)
		sDirect += "N";
	if (start.x > end.x)
		sDirect += "W";
	if (start.y < end.y)
		sDirect += "S";
	if (start.x < end.x)
		sDirect += "E";

	ObiectDirection direct = ObiectDirection::STOP;

	direct = ObiectNameSpace::StringToObiectDirection(sDirect);

	return direct;
}

void Obiect::dig(sf::Time & dt) {
	v_actualAnimationTime += dt.asSeconds();
	if (v_speedMoveAnimation <= v_actualAnimationTime) {	//zmiana 
		++v_actualframe;
		v_actualAnimationTime = 0;
		if (v_actualframe >= 4) {
			v_actualframe = 0;
			++v_actualNumberOfHits;
		}
		v_sprite.setTextureRect((*(*p_attackFrames)[v_direction])[v_actualframe]);
		if (v_numberOfHitsToDigout <= v_actualNumberOfHits) {	//jezeli uderzono dopowiednia ilosc razy przerwij kopanie
			SetModeByType(ObiectNameSpace::ObiectMode::APPLY);
		}
	}
}

void Obiect::apply(sf::Time & dt) {
}

void Obiect::move(sf::Vector2f & arg) {
	v_circle.move(arg);
	v_sprite.move(arg);
}

void Obiect::setPosition(sf::Vector2f & arg) {
	v_circle.setPosition(arg);
	v_sprite.setPosition(arg);
}

void Obiect::setPosition(float x, float y) {
	v_circle.setPosition(x, y);
	v_sprite.setPosition(x, y);
}

sf::Vector2f Obiect::ObliczWektorKierunkowyJednostki(sf::Vector2f & start, sf::Vector2f & koniec) {
	sf::Vector2f pom(0, 0);
	pom.x = koniec.x - start.x;
	pom.y = koniec.y - start.y;

	if (pom.x < 0)
		pom.x = -1;
	else if (pom.x > 0)
		pom.x = 1;

	if (pom.y < 0)
		pom.y = -1;
	else if (pom.y > 0)
		pom.y = 1;

	return pom;
}

bool Obiect::LoadScript(const char * scriptFilename, const std::string & tableName) {
	using namespace luabridge;
	if (luaL_dofile(L, scriptFilename) == 0) { // script has opened
		LuaRef table = getGlobal(L, tableName.c_str());
		if (table.isTable()) {
			if (table["v_name"].isString()) {
				v_name = table["v_name"].cast<std::string>();
			} else {
				v_name = "Null";
			}
			if (table["v_isBuilding"].isNumber()) {
				v_isBuilding = table["v_isBuilding"].cast<int>();	//TODO: problem castowanie na bool nie dziala
			} else {
				v_isBuilding = 0;
			}
			if (table["v_speed"].isNumber()) {
				v_speed = table["v_speed"].cast<float>();
			} else {
				v_speed = 0;
			}
			if (table["v_speedMoveAnimation"].isNumber()) {
				v_speedMoveAnimation = table["v_speedMoveAnimation"].cast<float>();
			} else {
				v_speedMoveAnimation = 0;
			}
			if (table["v_speedDeathAnimation"].isNumber()) {
				v_speedDeathAnimation = table["v_speedDeathAnimation"].cast<float>();
			} else {
				v_speedDeathAnimation = 0;
			}
			if (table["v_speedAttackAnimation"].isNumber()) {
				v_speedAttackAnimation = table["v_speedAttackAnimation"].cast<float>();
			} else {
				v_speedAttackAnimation = 0;
			}
			if (table["v_defence"].isNumber()) {
				v_defence = table["v_defence"].cast<int>();
			} else {
				v_defence = 0;
			}
			if (table["v_strength"].isNumber()) {
				v_strength = table["v_strength"].cast<int>();
			} else {
				v_strength = 0;
			}
			if (table["v_life"].isNumber()) {
				v_life = table["v_life"].cast<int>();
			} else {
				v_life = 0;
			}
			if (table["v_maxLife"].isNumber()) {
				v_maxLife = table["v_maxLife"].cast<int>();
			} else {
				v_maxLife = 0;
			}
			if (table["v_mana"].isNumber()) {
				v_mana = table["v_mana"].cast<int>();
			} else {
				v_mana = 0;
			}
			if (table["v_maxMana"].isNumber()) {
				v_maxMana = table["v_maxMana"].cast<int>();
			} else {
				v_maxMana = 0;
			}
			if (table["v_timeBuilding"].isNumber()) {
				v_timeBuilding = table["v_timeBuilding"].cast<float>();
			} else {
				v_timeBuilding = 0;
			}
			if (table["v_foodCost"].isNumber()) {
				v_foodCost = table["v_foodCost"].cast<int>();
			} else {
				v_foodCost = 0;
			}
			if (table["v_size"].isNumber()) {
				v_size = table["v_size"].cast<int>();
				SetSize(v_size);
			} else {
				v_size = 0;
			}
			if (table["v_circleColor"].isTable()) {
				//cout << table["v_circleColor"].cast<LuaRef>()["r"].cast<int>() << endl;
				LuaRef tableColor = table["v_circleColor"];
				sf::Color col(tableColor["r"].cast<int>(), tableColor["g"].cast<int>(), tableColor["b"].cast<int>(), tableColor["alpha"].cast<int>());
				v_circle.setFillColor(col);
			} else {
				v_circle.setFillColor(sf::Color(0, 0, 0));
			}
			if (table["InitScripts"].isFunction()) {
				LuaFunc["InitScripts"] = std::make_shared<luabridge::LuaRef>(table["InitScripts"]);
			} else {
				throw("Blad obiektu " + v_name + " nie mozna wczytac funkcji InitScript z pliku lua");
			}
		} else {
			return false;
		}
	} else {
		return false;
	}
	return true;
}