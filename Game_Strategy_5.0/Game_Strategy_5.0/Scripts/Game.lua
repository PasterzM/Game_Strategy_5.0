print("Game.lua")


Window = {
  v_width = 800,
  v_height = 800,
  
  showData = function(obi)
			print("-------------------------------------------------")
			print("Window:showData")
      print("obi.width ",obi.width)
      print("obi.height",obi.height)
			print("-------------------------------------------------")
  end,
    
  kill = function(obi)
    print("-------------------------------------------------")
    print("Game:kill")
    --obi.life = 0
    print("-------------------------------------------------")
  end,
  
  
  showScripts = function(obi)
    print("Game:printScriptsList")
    obi:printListofGameScripts()
  end,
  
  help = function(obi)
    print("Game:printScriptsList")
    obi:printListofGameScripts()
  end,
  
  Help = function(obi)
    print("Game:printScriptsList")
    obi:printListofGameScripts()
  end,
  
  showObiects = function(obi)
    print("Game:printShowObiects")
    obi:printListOfGameObiects()
  end,
  
  clear = function(obi)
      os.execute("cls")
  end,
  
  LoadScriptFunction = function(obi,name)
    print("Game:LoadScriptfunction")
    print(type(name))
    print(#name)
    obi:LoadScriptFunction(name)
  end,
  
  InitScripts = function(obi)
    table = {"showObiects","help","Help","showData","showScripts","InitMap","kill","SetSize"}
    
    for i = 1, #table do
      obi:LoadScriptFunction(table[i])
    end
  end,
  
  InitMap = function(obi)
    
  end,
  
   SetSize = function(obi,arg)
    obi:SetWindowSize(arg)
  end
}