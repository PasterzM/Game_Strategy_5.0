print("Obiect.lua")
--require "loadFile"

Obiect = {
		v_name = "Obiect",
		v_isBuilding = 0,
    v_size = 32,  --
    v_circleColor = {r = 91, g = 93, b = 116, alpha = 255},
	  v_circleSelectedColor= {r = 100, g = 100, b = 125, alpha = 255},
    v_circle = Circle,
		v_speed = 75.0,
    	
		v_speedMoveAnimation = 0.0,
		v_speedDeathAnimation = 0.0,
		v_speedAttackAnimation = 0.0,
		v_defence = 0.0,
		v_strength = 0.0,
		v_life = 0.0,
		v_maxLife = 0.0,
		v_mana = 0.0,
		v_maxMana = 0.0,
		v_timeBuilding = 0.0,
		v_foodCost = 0.0,
		
		showData = function(obi)
			print("-------------------------------------------------")
			print("Obiect:showData")
			print("v_name ",obi.v_name )
			print("v_isBuilding ",obi.v_isBuilding )
			print("v_speed ",obi.v_speed )
			print("v_speedMoveAnimation ",obi.v_speedMoveAnimation )
			print("v_speedDeathAnimation ",obi.v_speedDeathAnimation )
			print("v_speedAttackAnimation ",obi.v_speedAttackAnimation)
			print("v_defence ",obi.v_defence )
			print("v_strength ",obi.v_strength )
			print("v_life ",obi.v_life )
			print("v_maxLife ",obi.v_maxLife )
			print("v_mana ",obi.v_mana )
			print("v_maxMana ",obi.v_maxMana )
			print("v_timeBuilding ",obi.v_timeBuilding )
			print("v_foodCost ",obi.v_foodCost)			
			print("-------------------------------------------------")
		end,
    
  InitScripts = function(obi)
    table = {"showData","Interaction"};
    for i = 1, #table do
      obi:LoadScriptFunction(table[i])
    end
  end,
  
  Interaction = function(obi)
    print("Interaction")
  end
}

--io.write("v_name = "..Obiect["v_name"].."\n")
--io.write("Obiect type = "..type(Obiect).."\n")
--io.write("Obiect length = "..#Obiect.."\n")
--io.write("Obiect.v_size = "..Obiect["v_size"].."\n")

--DOBRZE NAPISANE
--filePath = "..\\filesTxt\\".. Obiect["v_name"]..".txt"
--LoadDataFromFile(Obiect,filePath,15)


--[[  --DO SPRAWDZENIA POL
for key,value in pairs(Obiect) do
  print(key,value)
end
]]