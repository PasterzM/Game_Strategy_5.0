print("Builder.lua")
--require "loadFile"

Builder = {
		v_name = "Builder",
		v_isBuilding = 0,
    	v_size = 32,
		v_speed = 75.0,
    v_circleColor = {r = 91, g = 93, b = 116, alpha = 255},
    v_circleSelectedColor= {r = 100, g = 100, b = 125, alpha = 255},
		v_speedMoveAnimation = 1.0,
		v_speedDeathAnimation = 0.0,
		v_speedAttackAnimation = 0.0,
		v_defence = 0.0,
		v_strength = 0.0,
		v_life = 100.0,
		v_maxLife = 100.0,
		v_mana = 0.0,
		v_maxMana = 0.0,
		v_timeBuilding = 0.0,
		v_foodCost = 0.0,
    v_mode = "ATTACKING",
		
		showData = function(obi)
			print("-------------------------------------------------")
			print("Obiect:showData")
			print("v_name ",obi.v_name )
			print("v_isBuilding ",obi.v_isBuilding )
			print("v_speed ",obi.v_speed )
			print("v_speedMoveAnimation ",obi.v_speedMoveAnimation )
			print("v_speedDeathAnimation ",obi.v_speedDeathAnimation )
			print("v_speedAttackAnimation ",obi.v_speedAttackAnimation)
			print("v_defence ",obi.v_defence )
			print("v_strength ",obi.v_strength )
			print("v_life ",obi.v_life )
			print("v_maxLife ",obi.v_maxLife )
			print("v_mana ",obi.v_mana )
			print("v_maxMana ",obi.v_maxMana )
			print("v_timeBuilding ",obi.v_timeBuilding )
			print("v_foodCost ",obi.v_foodCost)			
      print("v_mode ",obi.v_mode)
			print("-------------------------------------------------")
		end,
    
  InitScripts = function(obi)
    table = {"showData","Interaction"};
    for i = 1, #table do
      obi:LoadScriptFunction(table[i])
    end
    
    --Dodawanie przyciskow
    buttons = {"Farm","Barracks","Base"};
    
    for i = 1, #buttons do
      obi:AddToListOfButtons(buttons[i]);
    end    
  end,
  
  Interaction = function(obi,messManager)
    print("Interaction")
    local obserwowany = obi.v_observer
    if obi.v_player.v_id  == obserwowany.v_player.v_id then --jezeli sa w tej samej druzynie
      if obserwowany.v_name == "Barracks" then
        print("Kolizja nastapila z Barracks")
      else
        print("druzyna przeciwna")
      end
    elseif obserwowany.v_name == "GoldMine" then
        print("Kolizja nastapila z GoldMine")
        obi.v_mode = "DIGGING"
        print("v_mode ",obi.v_mode)
        --obserwowany.v_life = obserwowany.v_life - 100
        obi.v_TransportingMineType = "GOLD"
        m = Message("","",987,10.45,obi) --tworzenie obiektow, wywolanie konstruktora
        messManager:AddMessage( m )
    end
  end,
}

--io.write("v_name = "..Obiect["v_name"].."\n")
--io.write("Obiect type = "..type(Obiect).."\n")
--io.write("Obiect length = "..#Obiect.."\n")
--io.write("Obiect.v_size = "..Obiect["v_size"].."\n")

--DOBRZE NAPISANE
--filePath = "..\\filesTxt\\".. Obiect["v_name"]..".txt"
--LoadDataFromFile(Obiect,filePath,15)


--[[  --DO SPRAWDZENIA POL
for key,value in pairs(Obiect) do
  print(key,value)
end
]]
--Builder.Interaction()
--io.write("Obiect.v_size = "..Obiect["v_size"].."\n")