#include "MessageManager.h"
#include "Functions.h"

MessageManager* MessageManager::p_MessageManager = NULL;

MessageManager::MessageManager(luabridge::lua_State* L) {
	using namespace luabridge;

	getGlobalNamespace(L)
		.beginClass<MessageManager>("MessageManager")
		.addFunction("AddMessage", &MessageManager::AddMessage)
		.endClass()
		.beginClass<Message>("Message")
		.addConstructor<void(*)(string type, string contents, int id, float time, Obiect *obi)>()
		.endClass();
}

void MessageManager::Create(luabridge::lua_State* L) {
	if (p_MessageManager) {
		delete p_MessageManager;
		p_MessageManager = new MessageManager(L);
	} else {
		p_MessageManager = new MessageManager(L);
	}
}

MessageManager* MessageManager::GetObject() {
	return p_MessageManager;
}

void MessageManager::Destroy() {
	if (p_MessageManager) {
		delete p_MessageManager;
		p_MessageManager = NULL;
	}
}
MessageManager::MessageManager() {
}

const map<string, MetodyGame>& MessageManager::GetMesssageType() {
	return v_m_Functions;
}

void MessageManager::ExecuteMessage() {
	while (!v_Messages.empty()) {
		string type(v_Messages.begin()->v_MessageType);
		if (v_m_Functions.find(type) != v_m_Functions.end()) {
			//v_m_Functions[type]( *v_Messages.begin() );
			//TODO: uwaga nie dziala obsluga komunikatow
		}
	}
}

void MessageManager::AddMessageType(string name, MetodyGame fun) {
	v_m_Functions[name] = fun;
}

void MessageManager::AddMessage(Message mess) {
	v_Messages.push_back(mess);
}

const vector<Message>& MessageManager::GetMessages() const {
	return v_Messages;
}
