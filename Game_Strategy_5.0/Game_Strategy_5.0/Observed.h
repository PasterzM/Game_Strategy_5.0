#ifndef OBSERWOWANY_H_
#define OBSERWOWANY_H_
#include <vector>
#include "Observer.h"

class Observer;

//obserwowany
class Observed {
public:
	~Observed();
	void DodajObserwatora(Observer *pObs);
	virtual void UpdateObserver();
	void UsunObserwatora(Observer *pObs);
protected:
	std::vector<Observer *> Obserwatorzy;
};

#endif