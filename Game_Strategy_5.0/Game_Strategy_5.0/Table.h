#ifndef TABLE_H_
#define TABLE_H_

template <typename T>
class Table {
public:
	const int m_length;
	Table(const int value) :m_length(value < 0 ? 0 : value) {
		tab = new T[m_length];
	}
	Table(Table<T>& ref) :m_length(ref.m_length) {
		tab = new T[m_length];
		for (int i = 0; i < m_length;++i) {
			*(tab + i) = *(ref+i);
		}
	}
	Table(const int value,T init) :m_length(value < 0 ? 0 : value), init(init){
		tab = new T[m_length];
		for  (int i = 0; i < m_length; i++) {
			tab[i] = init;
		}
	}
	T& operator[](const int ind) {
		if (ind < m_length) {
			return tab[ind];
		} else {
			return tab[0];
		}
	}

	T& GetTable() {
		return *tab;
	}

	void reset() {
		for (int i = 0; i < m_length; i++) {
			tab[i] = init;
		}
	}
	~Table() {
		if (tab)
			delete[] tab;
		tab = NULL;
	}
private:
	T *tab = NULL;
	T init;
};

#endif