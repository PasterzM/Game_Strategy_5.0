#ifndef  MANAGER_TEXTURE_H_
#define MANAGER_TEXTURE_H_
#include <SFML\Graphics.hpp>
#include <map>

using namespace std;

class ManadzerTexture {
public:
	static ManadzerTexture* GetManadzer();
	static void Create();
	static void Remove();
	bool AddTexture(std::string name, std::string path);
	map<std::string, sf::Texture*>& GetMap();

	static string v_s_TexturePath;
	~ManadzerTexture();
private:
	ManadzerTexture() {};
	static ManadzerTexture* v_s_Obiect;
	map<std::string, sf::Texture*> v_m_texute;
};

#endif // ! MANAGER_TEXTURE_H_
