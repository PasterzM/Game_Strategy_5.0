#ifndef FUNCTION_H_
#define FUNCTION_H_

#include <vector>
#include <iterator>
#include <SFML\Graphics.hpp>
#include "Obiect.h"

using namespace std;



//funkcja sluzaca czszczeniu wektora, po nich nalezy uzyc metody clear
template<typename T>
void(*deletesVector)(T* n) = [](T *n) {
	delete n;
};

//czyszczenie map
template<typename T, typename C>
void(*deletesMap)(pair<T, C> p) = [](pair<T, C> p) {
	delete p.second;
};

//template<typename T>
//sf::Vector2<T> operator-(const sf::Vector2<T>& left, const sf::Vector2<T>& right) {
//	return sf::Vector2(left.x - right.x, left.y - right.y);
//}
template<typename T>
double Distance(const sf::Vector2<T>& left, const sf::Vector2<T>& right) {
	sf::Vector2f dis(left-right);
	return sqrt(dis.x*dis.x + dis.y*dis.y );
}

template<typename T>
sf::Vector2<T> operator/(const sf::Vector2<T>& left, const sf::Vector2<T>& right) {
	return sf::Vector2<T>(left.x/right.x,left.y/right.y);
}

//wyszukiwanie obiektu za pomoca nazwy
struct FindObjectByName {
	FindObjectByName(string name) :v_name(name) {
	}

	bool operator()(const Obiect& it) {
		if (it.GetName() == v_name)
			return true;
		return false;
	}
private:
	string v_name;
};


#endif