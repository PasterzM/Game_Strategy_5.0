#ifndef MESSAGE_H_
#define MESSAGE_H_

#include "Obiect.h"

class Message {
public:
	Message(string, string, int, float, Obiect*);

	string v_MessageType;
	string v_contents;
	int v_id;
	float v_time;
	Obiect* p_odbiect;

	ostream& operator<<(ostream& str) {
		str << "id : " << v_id << endl
			<< "--------------------------------------------" << endl
			<< "Objekt : " << p_odbiect->GetName() << endl
			<< "--------------------------------------------" << endl
			<< "typ: " << v_MessageType << endl
			<< "--------------------------------------------" << endl
			<< "czas: " << v_time << endl
			<< "--------------------------------------------" << endl
			<< "tresc: " << v_contents << endl
			<< "############################################" << endl;
		return str;
	};
};

#endif