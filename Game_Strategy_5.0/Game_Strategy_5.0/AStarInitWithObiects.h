#ifndef INITZCELEM_H_
#define INITZCELEM_H_
#include "AStarInit.h"

class AStarInitWithObiects :public AGwiazdkaInit {
public:
	void operator()(TileType **tab, int wysokosc, int dlugosc, sf::Vector2i &cel);
};

#endif
