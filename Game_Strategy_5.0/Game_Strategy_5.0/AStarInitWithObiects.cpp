#include "AStarInitWithObiects.h"

void AStarInitWithObiects::operator()(TileType ** tab, int wysokosc, int dlugosc, sf::Vector2i & cel) {
	Map* mapa = &Map::GetObiekt();

	for (int i = 0; i < wysokosc; i++) {	//inicjalizacja macierzy zamknietych tras
		for (int j = 0; j < dlugosc; j++) {
			if (!mapa->getPlytka(j, i).Wsk && mapa->getPlytka(j, i).type == TileType::GRASS)	//wszystko inne nie bedace trawa lub piaskiem jest blokada dla jednostki
				tab[j][i] = GRASS;
			else
				tab[j][i] = TileType::BLOCKED;
		}
	}


	//odblokowanie kawalka mapy z celem
	if (mapa->getPlytka((sf::Vector2f)cel).Wsk) {
		Obiect* wskObiektu = mapa->getPlytka((sf::Vector2f)cel).Wsk;

		int wielkosc = wskObiektu->Getcircle().getRadius() * 2;
		wielkosc /= Map::GetWielkoscPlytki().x;

		sf::Vector2i pozNaMacierzy((sf::Vector2i)wskObiektu->Getcircle().getPosition());
		pozNaMacierzy.x /= Map::GetWielkoscPlytki().x;
		pozNaMacierzy.y /= Map::GetWielkoscPlytki().y;

		for (int i = pozNaMacierzy.x; i<pozNaMacierzy.x + wielkosc; i++)
			for (int j = pozNaMacierzy.y; j < pozNaMacierzy.y + wielkosc; j++) {
				tab[i][j] = TileType::GRASS;
			}
	}
}
