#ifndef MAP_H_
#define MAP_H_

#include <SFML\Graphics.hpp>
#include <iostream>
#include <fstream>
#include <vector>
#include "Tile.h"

using namespace std;

class Map {
public:
	static const sf::Vector2i TEXTURE;
	static void Stworz();
	static void Zniszcz();
	static Map& GetObiekt();

	void CreateMap(std::string & filename,const int tileSize);

	void showMap();
	void showWskMap();
	Tile& getPlytka(int x, int y);
	Tile& getPlytka(sf::Vector2f &ref);
	Tile& getPlytka(sf::Vector2i &ref);
	void Rysuj(sf::RenderWindow &window);

	const int& GetDlugosc();
	const int& GetWysokosc();

	~Map();

	static sf::Vector2i WielkoscPlytki;

	const static sf::Vector2i& GetWielkoscPlytki();
private:
	Obiect& GetJednostka(sf::Vector2i& ind) { return *(Plytka[ind.x][ind.y].Wsk); };
	Obiect& GetJednostka(int x, int y) { return *(Plytka[x][y].Wsk); };

	static Map* Obiekt;

	Tile **Plytka;
	int Dlugosc;
	int Wysokosc;

	sf::IntRect textureRect[20][19];

	////////////////////////
	//	Brak konstruktora domyslnego
	////////////////////////
	Map();
};

#endif