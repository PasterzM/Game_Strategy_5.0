//#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#include <imgui.h>
#include <imgui-SFML.h>
#include <climits>
#include <string>
#include "Editor.h"
#include "Table.h"
#include "Game.h"
#include "LinesMaps.h"

static list<string> list_history;
static list<string>::reverse_iterator it_list_string;

static const char* p_tableDirection[] = { "STOP","N","NE","E","SE","S","SW","W","NW","N" };
static const int s_numberOfDirections = 9;

Editor::Editor(vector<Obiect>* vectObicts = NULL, map<string, Obiect*>* vecPaterns = NULL) : window(sf::VideoMode(330, 700), "Editor"),
v_circle(0.0f),
v_isLines(true),
v_isPanelView(true),
p_vecObiect(vectObicts),
v_vecObiectsPaterns(vecPaterns),
p_vecSelectedObiect(NULL),
v_selectedTexture(NULL),
v_textureMoveShape(0, 0, 0, 0),
v_p_interface(NULL),
v_buttonPos(0, 0),
v_buttonSize(1, 1),
v_numberOfButtons("numbers of buttons : "),
v_p_ButtonManadzer(NULL),
v_selectedButton(NULL),
v_p_SelectedButton(NULL),
p_constructionAnimation(NULL),
p_rectConstuctionAnimation(NULL),
selectedSound(NULL) {
	window.setPosition(sf::Vector2i(0, 0));
	LoadFiles();
	v_selectedTexture = new Table<bool>(ManadzerTexture::GetManadzer()->GetMap().size(), false);
	v_selectedButton = new Table<bool>(ManadzerTexture::GetManadzer()->GetMap().size(), false);
	selectedSound = new Table<bool>(SoundManager::GetManadzer()->GetMap().size(), false);

	p_constructionAnimation = new Table<bool>(3);
	p_rectConstuctionAnimation = new Table<sf::FloatRect>(3);

	if (!v_vecObiectsPaterns->empty()) {	//poczatkowa wartosc pozycji ikony
		v_textureIconPosition = v_vecObiectsPaterns->begin()->second->GetSriteIcon().getPosition();
		v_IconScale = v_vecObiectsPaterns->begin()->second->GetSriteIcon().getScale();
	}
}

Editor::~Editor() {
	if (v_selectedTexture)
		delete v_selectedTexture;
	v_selectedTexture = NULL;

	(p_constructionAnimation ? delete p_constructionAnimation : p_constructionAnimation = NULL);
	p_constructionAnimation = NULL;

	(p_rectConstuctionAnimation ? delete p_rectConstuctionAnimation : p_rectConstuctionAnimation = NULL);
	p_rectConstuctionAnimation = NULL;

	if (v_selectedButton)
		delete v_selectedButton;
	v_selectedButton = NULL;
}

const sf::RenderWindow & Editor::GetWindow() {
	return window;
}

void Editor::addTextToConsole(const std::string arg) {
	list_output.push_back(arg);
}

void Editor::ExecuteEditor() {
	sf::Event event;
	sf::Clock deltaClock;
	while (window.pollEvent(event)) {
		ImGui::SFML::ProcessEvent(event);
		if (event.type == sf::Event::Closed) {
			Game::StatusGry = EXIT;
			window.close();
		}
		if (event.type == sf::Event::KeyPressed) {
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
				//historia konsoli
				if (it_list_string != list_history.rend()) {
					strcpy_s(v_script, (*(it_list_string)).c_str());
					++it_list_string;
				} else if (!list_history.empty()) {
					strcpy_s(v_script, ((*list_history.begin()).c_str()));
					it_list_string = list_history.rbegin();
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
				//historia konsoli
				if (it_list_string != list_history.rbegin()) {
					--it_list_string;
					strcpy_s(v_script, (*it_list_string).c_str());
				} else if (!list_history.empty()) {
					strcpy_s(v_script, ((list_history.back()).c_str()));
					it_list_string = list_history.rend();
				}
			}
			if (event.key.code == sf::Keyboard::Return) {
				v_isPressEnter = true;
			}

		}
	}
	ImGui::SFML::Update(window, deltaClock.restart());

	ImGui::SetNextWindowPos(ImVec2(0, 0));
	ImGui::SetNextWindowSize(ImVec2(window.getSize().x, window.getSize().y));// , ImGuiSetCond_FirstUseEver);
	ImGui::Begin("Menu:", 0, ImGuiWindowFlags_ShowBorders);
	//ImGui::ShowTestWindow();

	if (ImGui::CollapsingHeader("Scripts")) {
		//TODO:konsola do wczytywania skryptow
		ImGui::PushStyleVar(ImGuiStyleVar_ChildWindowRounding, 0.5f);
		ImGui::BeginChild("Sub1", ImVec2(window.getSize().x, window.getSize().y - 100), true);
		for (list<string>::iterator it = list_output.begin(); it != list_output.end(); ++it) {
			ImGui::Text("%s", (*it).c_str());
		}
		ImGui::EndChild();
		ImGui::PopStyleVar();
		ImGui::InputText("", v_script, 255);	//TODO: poprawic wielkosc bufora
		ImGui::SameLine();
		//obsluga przycisku 
		if (v_isPressEnter || ImGui::Button("send")) {
			if (strcmp(v_script, "") != 0)
				list_output.push_back(v_script);

			toReturn = v_script;
			list_history.push_back(toReturn);
			it_list_string = list_history.rbegin();
			fillTableChar(v_script, '\0', 255);
			v_isPressEnter = false;
		}
	}
	if (ImGui::CollapsingHeader("Obiects") && p_vecSelectedObiect) {
		//ImGui::Text();
		ImGui::Text(p_vecSelectedObiect->GetName().c_str());
		ImGui::PushStyleVar(ImGuiStyleVar_ChildWindowRounding, 0.5f);
		ImGui::BeginChild("Sub2", ImVec2(window.getSize().x*0.95, window.getSize().y*0.25), true);

		int i = 0;
		ManadzerTexture *pManadzerTexture = ManadzerTexture::GetManadzer();
		for (map<string, sf::Texture*>::iterator it = pManadzerTexture->GetMap().begin(); it != pManadzerTexture->GetMap().end(); ++it) {
			if (ImGui::Selectable(it->first.c_str(), &(*v_selectedTexture)[i])) {
				for (int j = 0; j < v_selectedTexture->m_length; ++j) {	//odznaczanie
					if (i != j)
						(*v_selectedTexture)[j] = false;
					else {
						v_pSelectedTexture = (it->second);
						p_vecSelectedObiect->SetTexture(*v_pSelectedTexture);
						p_vecSelectedObiect->SetSprite(*v_pSelectedTexture);
						p_vecSelectedObiect->GetSprite().setTextureRect((sf::IntRect)v_textureMoveShape);

						//nadanie sprita wzorowi
						v_vecObiectsPaterns->at(p_vecSelectedObiect->GetName())->SetSprite(*v_pSelectedTexture);
						v_vecObiectsPaterns->at(p_vecSelectedObiect->GetName())->GetSprite().setTextureRect((sf::IntRect)v_textureMoveShape);
						v_vecObiectsPaterns->at(p_vecSelectedObiect->GetName())->SetSpritePath(ManadzerTexture::v_s_TexturePath + "/" + it->first);
						//cout << ManadzerTexture::v_s_TexturePath << "/" << it->first << ".png"<<endl;	//OK
						//zmiana textury dla kazdego obiektu w grze
						for (auto it = p_vecObiect->begin(); it != p_vecObiect->end(); ++it) {
							if (it->GetName() == p_vecSelectedObiect->GetName()) {
								it->SetSprite(*v_pSelectedTexture);
							}
						}
					}
				}

			}
			++i;
			//p_vecSelectedObiect->begin()->SetSprite(v_selectedTexture);
		}
		ImGui::EndChild();
		ImGui::PopStyleVar();
		ImGui::BeginGroup();

		if (ImGui::DragFloat4("Icon size", (float*)&v_textureIcon, 0.1f, 0.0f, 1000.0f, NULL, 2.0f)) {
			for (vector<Obiect>::iterator it = p_vecObiect->begin(); it != p_vecObiect->end(); ++it) {
				if (it->GetName() == p_vecSelectedObiect->GetName()) {
					it->GetSriteIcon().setTextureRect((sf::IntRect)v_textureIcon);
				}
			}

			if (v_vecObiectsPaterns->find(p_vecSelectedObiect->GetName()) != v_vecObiectsPaterns->end()) {
				v_vecObiectsPaterns->at(p_vecSelectedObiect->GetName())->GetSriteIcon().setTextureRect((sf::IntRect)v_textureIcon);
			}
		}

		//wyswietlanie grafiki do animacji budowy
		for (int i = 0; i < p_constructionAnimation->m_length; ++i) {
			ImGui::Image(p_vecSelectedObiect->GetTexture(), sf::Vector2f(100, 100), (*p_rectConstuctionAnimation)[i]);
			ImGui::SameLine();
		}ImGui::Text("");
		for (int i = 0; i < p_constructionAnimation->m_length; ++i) {
			ImGui::DragFloat4("fundament animation rect " + i, (float*)&(*p_rectConstuctionAnimation)[i], 0.1f, 0.0f, 300.0f);
		}

		AnimationEditor("Ruch", v_selectedFrameFromAnimation, v_countFrameInAnimation, v_indOfDirection, v_textureMoveShape, p_vecSelectedObiect->GetFrameAnimationTable());
		AnimationEditor("Atak", v_selectedFrameFromAttack, v_countFrameInAttack, v_indOfAttack, v_textureAttackShape, p_vecSelectedObiect->GetFrameAttackTable());
		AnimationEditor("Smierc", v_selectedFrameDeath, v_countFrameInDeath, v_indOfDeath, v_textureDeathShape, p_vecSelectedObiect->GetFrameDeathTable());

		//animacja ruchu
		//ImGui::Text("Animacje Ruchu :");
		//if (p_vecSelectedObiect->GetSprite().getTexture())
		//	ImGui::Image(*p_vecSelectedObiect->GetSprite().getTexture(), sf::Vector2f(150, 150), v_textureMoveShape);

		//if (ImGui::Combo("comboDirection", &v_indOfDirection, p_tableDirection, s_numberOfDirections)) {	//wybor kierunku obiektu
		//	v_countFrameInAnimation = (*p_vecSelectedObiect->GetFrameAnimationTable()[v_indOfDirection]).m_length - 1;
		//}
		//if (ImGui::SliderInt("chosenFrame", &v_selectedFrameFromAnimation, 0, v_countFrameInAnimation)) {
		//	v_textureMoveShape = (sf::FloatRect)(*p_vecSelectedObiect->GetFrameAnimationTable()[v_indOfDirection])[v_selectedFrameFromAnimation];
		//}

		//if (ImGui::DragFloat4("textureRect", (float*)&v_textureMoveShape, 1.0f, 0.0f, 1000.0f)) {
		//	for (vector<Obiect>::iterator it = p_vecObiect->begin(); it != p_vecObiect->end(); ++it) {
		//		if (it->GetName() == p_vecSelectedObiect->GetName()) {
		//			//it->SetTexture(*v_pSelectedTexture);
		//			//it->SetSprite(*v_pSelectedTexture);
		//			//it->GetSprite().setTextureRect((sf::IntRect)v_textureShape);
		//			(*it->GetFrameAnimationTable()[v_indOfDirection])[v_selectedFrameFromAnimation] = (sf::IntRect)v_textureMoveShape;
		//			v_vecObiectsPaterns->at(p_vecSelectedObiect->GetName())->GetSprite().setTextureRect((sf::IntRect)v_textureMoveShape);
		//			for (auto it = p_vecObiect->begin(); it != p_vecObiect->end(); ++it) {
		//				if (it->GetName() == p_vecSelectedObiect->GetName()) {
		//					it->GetSprite().setTextureRect((sf::IntRect)v_textureMoveShape);
		//				}
		//			}
		//		}
		//	}
		//}

		if (ImGui::DragFloat("frame speed", &v_AnimationSpeed, 0.001, 0, 60)) {
			for (vector<Obiect>::iterator it = p_vecObiect->begin(); it != p_vecObiect->end(); ++it) {
				it->SetMoveAnimationSpeed(v_AnimationSpeed);
			}
			for (map<string, Obiect*>::iterator it = v_vecObiectsPaterns->begin(); it != v_vecObiectsPaterns->end(); ++it) {
				it->second->SetMoveAnimationSpeed(v_AnimationSpeed);
			}
		}
		//koniec animacji ruchu

		ImGui::EndGroup();
	}
	if (ImGui::CollapsingHeader("Game")) {
		ImGui::Checkbox("is map lines", &v_isLines); ImGui::SameLine(150);
		Game::GetLinesMaps().SetViewLines(v_isLines);

		if (ImGui::Checkbox("is there panel", &v_isPanelView)) {// ImGui::SameLine(150);
			v_p_interface->SetIsPanelView(v_isPanelView);
			if (v_isPanelView) {
				v_p_interface->GetGameView().setViewport(sf::FloatRect(0.3f, 0.0f, 1.0f, 1.0f));
				//v_p_interface->GetGameView().reset(sf::FloatRect(0, 0, 800, 600));
			} else {
				v_p_interface->GetGameView().setViewport(sf::FloatRect(0.0f, 0.0f, 1.0f, 1.0f));
			}
		}
		ImGui::Text("Game View position : "); ImGui::SameLine();
		int viewX(v_p_interface->GetGameView().getCenter().x);
		int viewY(v_p_interface->GetGameView().getCenter().y);

		string viewPosString;
		sprintf((char*)viewPosString.c_str(), " x : %d y : %d", viewX, viewY);

		ImGui::Text(viewPosString.c_str());

		sprintf((char*)viewPosString.c_str(), " x : %d y : %d", viewX / Map::GetWielkoscPlytki().x, viewY / Map::GetWielkoscPlytki().y);
		ImGui::Text(viewPosString.c_str());

		if (ImGui::DragFloat2("Icons position", (float*)&v_textureIconPosition, 0.1f, 0.0f, 1000.0f, NULL, 2.0f)) {
			for (vector<Obiect>::iterator it = p_vecObiect->begin(); it != p_vecObiect->end(); ++it) {
				it->GetSriteIcon().setPosition(v_textureIconPosition);
			}
			for (map<string, Obiect*>::iterator it = v_vecObiectsPaterns->begin(); it != v_vecObiectsPaterns->end(); ++it) {
				it->second->GetSriteIcon().setPosition(v_textureIconPosition);

			}
		}
		if (ImGui::DragFloat2("Icons scale", (float*)&v_IconScale, 0.01f, 0.0f, 100.0f, NULL, 2.0f)) {
			for (vector<Obiect>::iterator it = p_vecObiect->begin(); it != p_vecObiect->end(); ++it) {
				it->GetSriteIcon().setScale(v_IconScale);
			}
			for (map<string, Obiect*>::iterator it = v_vecObiectsPaterns->begin(); it != v_vecObiectsPaterns->end(); ++it) {
				it->second->GetSriteIcon().setScale(v_IconScale);
			}
		}

		//Przyciski
		ImGui::Text(v_numberOfButtons.c_str());

		if (ImGui::InputInt("button number", &buttonInd)) {
			if (buttonInd >= 0 && buttonInd < v_p_interface->GetButtonTable().m_length) {
				v_buttonPos = v_p_interface->GetButtonTable()[buttonInd].getPosition();
				v_buttonRect = v_p_interface->GetButtonTable()[buttonInd].getTextureRect();
			} else {
				buttonInd = 0;
			}
		}
		if (ImGui::DragFloat2("button pos", (float*)&v_buttonPos, 0.1f, 0.0f, 1000.0f, NULL, 2.0f)) {
			v_p_interface->GetButtonTable()[buttonInd].setPosition(v_buttonPos);
		}

		if (ImGui::DragFloat2("buttons scale", (float*)&v_buttonSize, 0.01f, 0.1f, 100.0f, NULL, 2.0f)) {
			for (int i = 0; i < v_p_interface->GetButtonTable().m_length; ++i) {
				v_p_interface->GetButtonTable()[i].setScale(v_buttonSize);
			}
		}
		//ikony
		ImGui::Text("Menadzer Przyciskow");

		int i = 0;
		if (ImGui::BeginChild("icons1", ImVec2(window.getSize().x*0.95, window.getSize().y*0.25), true)) {
			//TODO:scaleButtonRect powinien przyjmowac wartosci danego przycisku
			for (map<string, Button*>::iterator it = v_p_ButtonManadzer->begin(); it != v_p_ButtonManadzer->end(); ++it) {
				if (ImGui::Selectable(it->first.c_str(), &(*v_selectedButton)[i])) {
					for (int j = 0; j < v_selectedButton->m_length; ++j) {	//odznaczanie
						if (i != j) {
							(*v_selectedButton)[j] = false;
						} else {
							v_p_SelectedButton = (it->second);
							v_ButtonTextureRect = (sf::FloatRect)v_p_SelectedButton->getTextureRect();
							v_buttonType = v_p_SelectedButton->GetType();
						}
					}
				}
				++i;
			}
		}
		ImGui::EndChild();

		if (v_p_SelectedButton) {
			//ImGui::Image(*v_p_SelectedButton->getTexture(), sf::Vector2f(150,150),sf::FloatRect(0,0,100,100));
			ImGui::Image(*v_p_SelectedButton->getTexture(), sf::Vector2f(150, 150), v_ButtonTextureRect);


			if (ImGui::DragFloat4("IcontextureRect", (float*)&v_ButtonTextureRect, 0.1f, 0.0f, 1000.0f, NULL, 2.0f)) {
				v_p_SelectedButton->setTextureRect((sf::IntRect)v_ButtonTextureRect);
			}

			ImGui::Text("Ustalanie rodzaju przycisku :");
			if (ImGui::RadioButton("Budynek", &v_buttonType, 0)) {
				v_p_SelectedButton->SetType(ButtonType::BUILDING);
			}ImGui::SameLine();
			if (ImGui::RadioButton("Jednostka", &v_buttonType, 1)) {
				v_p_SelectedButton->SetType(ButtonType::UNIT);
			}ImGui::SameLine();
			if (ImGui::RadioButton("zdolnosc", &v_buttonType, 2)) {
				v_p_SelectedButton->SetType(ButtonType::CAPACITY);
			}
		}

		SetStringsPosition();
		SetPlayerPosition();

		ImGui::Text("Menadzer dzwiekow");
		ImGui::BeginChild("soundsList", ImVec2(window.getSize().x*0.95, window.getSize().y*0.25), true);
		map<string, sf::Sound*>* mapp = &SoundManager::GetManadzer()->GetMap();
		i = 0;
		for (map<string, sf::Sound*>::iterator it = mapp->begin(); it != mapp->end(); ++it) {
			if (ImGui::Selectable(it->first.c_str(), &(*selectedSound)[i])) {
				p_selectedSound = it->second;
				for (int j = 0; j < selectedSound->m_length; ++j) {
					if (i != j) {
						(*selectedSound)[j] = false;
					}
				}
			}
			++i;
		}

		//ImGui::Selectable();
		ImGui::EndChild();

		if (ImGui::Button("Play sound")) {
			if (p_selectedSound)
				p_selectedSound->play();
		}
	}

	/*if (ImGui::Button("Restart Map")) {
		Game::StatusGry = RESTARTMAP;
		Exit();
	}	ImGui::SameLine();*/


	if (ImGui::Button("Restart")) {
		Game::StatusGry = RESTART;
		Exit();
	}ImGui::SameLine();

	if (ImGui::Button("Exit")) {
		Game::StatusGry = EXIT;
		Exit();
	}
	ImGui::End(); // end v_window

	window.clear(sf::Color::Black); // fill background with color
	ImGui::Render();
	window.draw(v_circle);
	window.display();
}

void Editor::Exit() {
	window.close();
}

void Editor::SetObiectToEdit(Obiect * p) {
	p_vecSelectedObiect = p;
	v_selectedTexture->reset();
	v_textureMoveShape = (sf::FloatRect)p->GetTextureRect();
	v_pSelectedTexture = &p->Gettexture();
	SetTextureByObiectTexture(p->GetName());
	v_p_interface->SetObiectToView(p);
	v_textureIcon = (sf::FloatRect)p->GetSriteIcon().getTextureRect();
	v_textureMoveShape = (sf::FloatRect)p->GetSprite().getTextureRect();

	int v_selectedFrameFromAnimation;	//aktualnie modyfikowana klatka animacji
	int v_countFrameInAnimation;	//ilosc klatek w animacji
	int v_indOfDirection;			//indeks kierunku aktualnie modyfikowany

	v_AnimationSpeed = p->GetspeedMoveAnimation();
}

void Editor::ClearSelectedObiect() {
	p_vecSelectedObiect = NULL;
	v_pSelectedTexture = NULL;
	v_textureMoveShape.left = 0;
	v_textureMoveShape.top = 0;
	v_textureMoveShape.width = 0;
	v_textureMoveShape.height = 0;
}

void Editor::SetPanelToEdit(Interface &arg) {
	v_p_interface = &arg;
	stringstream ss;
	ss << v_p_interface->GetButtonTable().m_length;
	v_numberOfButtons += ss.str();

	//ustawienia przyciskow
	v_buttonSize = v_p_interface->GetButtonTable()[0].getScale();
	v_buttonRect = v_p_interface->GetButtonTable()[0].getTextureRect();

	//pozycje napisow
	PrintObiect& p = v_p_interface->GetObiectToView();
	v_namePosition.x = p.GetName().getPosition().x;
	v_namePosition.y = p.GetName().getPosition().y;
	v_namePosition.z = p.GetName().getCharacterSize();

	v_lifePosition.x = p.GetLife().getPosition().x;
	v_lifePosition.y = p.GetLife().getPosition().y;
	v_lifePosition.z = p.GetLife().getCharacterSize();

	v_manaPosition.x = p.GetMana().getPosition().x;
	v_manaPosition.y = p.GetMana().getPosition().y;
	v_manaPosition.z = p.GetMana().getCharacterSize();

	v_PlayerPosition.x = p.GetPlayer().getPosition().x;
	v_PlayerPosition.y = p.GetPlayer().getPosition().y;
	v_PlayerPosition.z = p.GetPlayer().getCharacterSize();
}

void Editor::setButtonManadzer(map<string, Button*>* arg) {
	v_p_ButtonManadzer = arg;
}

void Editor::CreateEditorInLua(luabridge::lua_State * L) {
	getGlobalNamespace(L)
		.beginClass<Editor>("Editor")
		.addFunction("EditorPrint", &Editor::addTextToConsole)
		.endClass();
}

void Editor::LoadFiles() {
}

void Editor::SetTextureByObiectTexture(string& texture) {
	/*
	int i = 0;
	for (map<string, sf::Texture>::iterator it = v_map_Textures.begin(); it != v_map_Textures.end(); ++it) {
		if (texture == it->first) {
			(*selectedTexture)[i] = true;
			break;
		} else
			i++;
	}
	*/
}

void Editor::SetStringsPosition() {

	if (ImGui::DragInt("character size", &v_characterSize, 0.01f, 0.1f, 1000.0f, NULL)) {
		v_p_interface->GetObiectToView().SetCharacterSize(v_characterSize);
	}

	if (ImGui::DragFloat3("name position", (float*)&v_namePosition, 0.01f, 0.1f, 1000.0f, NULL, 2.0f)) {
		v_p_interface->GetObiectToView().GetName().setPosition(v_namePosition.x, v_namePosition.y);
		v_p_interface->GetObiectToView().GetName().setCharacterSize(v_namePosition.z);
	}
	if (ImGui::DragFloat3("life position", (float*)&v_lifePosition, 0.01f, 0.1f, 1000.0f, NULL, 2.0f)) {
		v_p_interface->GetObiectToView().GetLife().setPosition(v_lifePosition.x, v_lifePosition.y);
		v_p_interface->GetObiectToView().GetLife().setCharacterSize(v_lifePosition.z);
	}
	if (ImGui::DragFloat3("mana position", (float*)&v_manaPosition, 0.01f, 0.1f, 1000.0f, NULL, 2.0f)) {
		v_p_interface->GetObiectToView().GetMana().setPosition(v_manaPosition.x, v_manaPosition.y);
		v_p_interface->GetObiectToView().GetMana().setCharacterSize(v_manaPosition.z);
	}
	if (ImGui::DragFloat3("playerName position", (float*)&v_PlayerPosition, 0.01f, 0.1f, 1000.0f, NULL, 2.0f)) {
		v_p_interface->GetObiectToView().GetPlayer().setPosition(v_PlayerPosition.x, v_PlayerPosition.y);
		v_p_interface->GetObiectToView().GetPlayer().setCharacterSize(v_PlayerPosition.z);
	}
}

void Editor::AnimationEditor(string name, int & selectedFrame, int & countFrame, int & indDirection, sf::FloatRect& textureShape, Table<Table<sf::IntRect>*>& table) {
	string n("Animacje" + name + " :");
	ImGui::Text(n.c_str());
	if (p_vecSelectedObiect->GetSprite().getTexture())
		ImGui::Image(*p_vecSelectedObiect->GetSprite().getTexture(), sf::Vector2f(150, 150), textureShape);	//pobieranie calej textury do wywietlenia ael wyswietla tylko kawalek oznaczony textureShape

	n = "comboDirection " + name;
	if (ImGui::Combo(n.c_str(), &v_indOfDirection, p_tableDirection, s_numberOfDirections)) {	//wybor kierunku obiektu
		countFrame = (*p_vecSelectedObiect->GetFrameAnimationTable()[v_indOfDirection]).m_length - 1;
	}

	n = "chosenFrame " + name;
	if (ImGui::SliderInt(n.c_str(), &selectedFrame, 0, countFrame)) {
		textureShape = (sf::FloatRect)(*p_vecSelectedObiect->GetFrameAnimationTable()[v_indOfDirection])[selectedFrame];
	}

	n = "textureRect " + name;
	if (ImGui::DragFloat4(n.c_str(), (float*)&textureShape, 1.0f, 0.0f, 1000.0f)) {
		for (vector<Obiect>::iterator it = p_vecObiect->begin(); it != p_vecObiect->end(); ++it) {
			if (it->GetName() == p_vecSelectedObiect->GetName()) {
				if (name == "Ruch") {
					(*it->GetFrameAnimationTable()[v_indOfDirection])[selectedFrame] = (sf::IntRect)textureShape;	//w obiekcie, ustalanie na tablicy kordynatow wybrane wartosci	
				}
				if (name == "Atak") {
					(*it->GetFrameAttackTable()[v_indOfDirection])[selectedFrame] = (sf::IntRect)textureShape;	//w obiekcie, ustalanie na tablicy kordynatow wybrane wartosci	
				}
				if (name == "Smierc") {
					(*it->GetFrameDeathTable()[v_indOfDirection])[selectedFrame] = (sf::IntRect)textureShape;	//w obiekcie, ustalanie na tablicy kordynatow wybrane wartosci
				}
				v_vecObiectsPaterns->at(p_vecSelectedObiect->GetName())->GetSprite().setTextureRect((sf::IntRect)textureShape);
				/*for (auto it = p_vecObiect->begin(); it != p_vecObiect->end(); ++it) {
					if (it->GetName() == p_vecSelectedObiect->GetName()) {
						it->GetSprite().setTextureRect((sf::IntRect)v_textureMoveShape);
					}
				}*/
			}
		}
	}
}

void Editor::SetPlayerPosition() {
	if (ImGui::DragFloat2("Player Gold Position", (float*)&v_playerGoldPosition, 1.0f, 0, 800)) {
		v_p_interface->GetPlayerGold().setPosition(v_playerGoldPosition);
	}
	if (ImGui::DragFloat2("Player Metal Position", (float*)&v_playerMetalPosition, 1.0f, 0, 800)) {
		v_p_interface->GetPlayerMetal().setPosition(v_playerMetalPosition);
	}
	if (ImGui::DragFloat2("Player Food Position", (float*)&v_playerFoodPosition, 1.0f, 0, 800)) {
		v_p_interface->GetPlayerFood().setPosition(v_playerFoodPosition);
	}

}
