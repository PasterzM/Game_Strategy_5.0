#include "Observed.h"

Observed::~Observed() {
	for (std::vector<Observer *>::iterator it = Obserwatorzy.begin(); it != Obserwatorzy.end(); ++it)
		(*it)->SetObserver(NULL);
}

void Observed::DodajObserwatora(Observer * pObs) {
	Obserwatorzy.push_back(pObs);
}

void Observed::UpdateObserver() {
	for (std::vector<Observer *>::iterator it = Obserwatorzy.begin(); it != Obserwatorzy.end(); ++it)
		(*it)->AktualizujObiektObserwowany();
}

void Observed::UsunObserwatora(Observer * pObs) {
	for (auto it = Obserwatorzy.begin(); it != Obserwatorzy.end(); ++it) {
		if ((*it) == pObs) {
			Obserwatorzy.erase(it);
			return;
		}
	}
}
