#ifndef GAME_H_
#define GAME_H_

#include <iostream>
#include <LuaBridge.h>
#include <vector>
#include <string>
#include <iterator>
#include <algorithm>

extern "C" {
# include "lua.h"
# include "lauxlib.h"
# include "lualib.h"
}

#include "Map.h"
#include "Obiect.h"
#include "Editor.h"
#include "selection.h"
#include "LinesMaps.h"
#include "Interface.h"
#include "Player.h"
#include "FindPath.h"
#include "Player.h"
#include "TextureManager.h"
#include "AStarInitWithObiects.h"
#include "Interface.h"
#include "MessageManager.h"
#include "SoundManager.h"



using namespace std;

enum GameStatus {
	MENU,
	PLAY,
	RESTART,
	RESTARTMAP,
	EXIT
};

namespace MouseNameSpace {

	enum MouseType {
		SELECTED,
		PRESS,
		REALESE,
		NOTHING
	};

};
class Game {
public:
	Game();
	~Game();

	static GameStatus StatusGry;
	static LinesMaps& GetLinesMaps();
private:
	sf::RenderWindow v_window;
	Editor *v_EditorWindow;

	map<string, Obiect*>::iterator it_map_obiect;
	vector<Obiect>::iterator it_vec_obiects;
	vector<Obiect*>::iterator v_vec_p_Obiects;

	vector<Obiect> v_vec_Obiects;	//obiekty na mapie
	vector<Obiect*> v_vec_SelectedObiects;	//zaznaczone obiekty
	vector<Player> v_vec_Players;
	map<string, Obiect*> Paterns;
	MouseNameSpace::MouseType v_MouseType;	//tryb w jakim znajduje sie konkretnie myszka

	sf::Vector2i pozMyszki;

	void GlownaPetla();
	void Wejscie();
	void Draw();
	void Aktualizuj(sf::Time &dt);

	void executeScript(string& script);

	//dane/wartosci dotyczace gry
	//void WczytajGlobalne(const std::string &filename);
	//void WczytajParametry(const std::string &filename);
	//void WczytajParametryGry(fstream &plik);
	//void WypiszParametryGry();

	void InitMap();
	void InitScripts();
	void InitButtons();
	void InitMessageManager();
	void InitSoundManager();
	//Editor edytor;

	//void WprowadzSciezkeDlaJednostek();
	//void ZaznaczanieJednostek();

	//int getWielkoscobiektu(string nazwaObiektu);

	void Menu();
	void LoadFile();
	//void LoadObiectValueFromFiles();
	//void LoadObiectValueFromFile();
	ostream& ShowParameter(ostream &out);

	//std::map<string, int> ParametryGry;
	map<string, string> map_MapMenager;
	map<string, string> map_scriptMenager;
	map<string, sf::Texture> map_SpritesMenager;
	//TODO: menadzer skrpytow?

	map<string, string>::iterator it_maps;
	map<string, string>::iterator it_scripts;
	//id wybranej mapy ktora bedzie uruchamiana
	static map<string, string>::iterator it_selectedMap;

	//LUA
	luabridge::lua_State *L;
	map<string, std::shared_ptr<luabridge::LuaRef>> LuaFunc;
	map<string, std::shared_ptr<luabridge::LuaRef>>::iterator it_map_LuaFunc;
	bool LoadScriptfunctionFromWindow(string tab);
	void executeGameScript(string& script, list<string>::iterator& it);
	void LoadTexture();
	void SetWindowSize(sf::Vector2u &ref);

	list<string>::iterator it_list_String;
	list<string> list_gameDictionary;

	void printListofGameScripts();
	void printListOfGameObiects();
	void addTextToConsole(const string& arg);

	int table[10];

	static LinesMaps linesMaps;

	bool isCollision(Obiect &ref, sf::RectangleShape &sel);

	FindPath v_AStar;

	void InitPlayers();

	std::shared_ptr<luabridge::LuaRef> v_LuaTable;

	sf::View v_gameView;
	sf::View v_panelView;

	Interface v_interface;

	void InitEditor();

	map<string, Button*> v_m_ButtonManager;
	static string v_s_FileButtonRectType;

	void saveConfiguration();

	int v_numberOfAnimationInBuild;

	Obiect& FindClosestObiect(string ObiectToFind, Obiect& ref);

	void StartAttacking(Message arg);
	void ProvidedResources(Message arg);
	void EndDigging(Message arg);
	void CreateObject(Message arg);
	void KillObject(Message arg);
};

#endif